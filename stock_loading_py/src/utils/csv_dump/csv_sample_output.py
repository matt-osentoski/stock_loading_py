'''
Created on Jun 7, 2010

@author: Matt.Osentoski
'''
import csv

from dao.stock_dao import StockDAO

BASE_DIR = "/tmp/sample"
csv_file = "%s/sp_500-11_2008_sample.csv" % (BASE_DIR)

dao = StockDAO()
stock_prices = dao.export_stock_prices("2008-11-01", "2008-11-31")
try:
    writer = csv.writer(open(csv_file, 'wb'))
    writer.writerow(["symbol", "period", "price_date", "opening_price", 
                 "high_price", "low_price", "closing_price", 
                 "adj_closing_price", "volume", "sma_5", "sma_10", "sma_20", 
                 "sma_50", "sma_100", "sma_200", "ema_5", "ema_10", "ema_20", 
                 "ema_50", "ema_100", "ema_200", "ema_12", "ema_26", 
                 "macd_12_26", "macd_12_26_signal_9", "macd_hist_12_26", 
                 "ppo_12_26", "ppo_12_26_signal_9", "ppo_hist_12_26", "rsi_14", 
                 "rsi_20", "rsi_30", "stoch_fast_k_10_3", "stoch_fast_d_10_3",
                 "stoch_slow_k_10_3", "stoch_slow_d_10_3",
                 "stoch_fast_k_14_3", "stoch_fast_d_14_3",
                 "stoch_slow_k_14_3", "stoch_slow_d_14_3", "atr_14",
                 "vol_sma_5", "vol_sma_10", "vol_sma_20", "vol_sma_50",
                 "vol_sma_100", "vol_sma_200"])
    for sp in stock_prices:
        writer.writerow([sp.symbol, sp.period, sp.price_date, sp.opening_price, 
                 sp.high_price, sp.low_price, sp.closing_price, 
                 sp.adj_closing_price, sp.volume, sp.sma_5, sp.sma_10, 
                 sp.sma_20, sp.sma_50, sp.sma_100, sp.sma_200, sp.ema_5, 
                 sp.ema_10, sp.ema_20, sp.ema_50, sp.ema_100, sp.ema_200, 
                 sp.ema_12, sp.ema_26, sp.macd_12_26, sp.macd_12_26_signal_9, 
                 sp.macd_hist_12_26, sp.ppo_12_26, sp.ppo_12_26_signal_9, 
                 sp.ppo_hist_12_26, sp.rsi_14, sp.rsi_20, sp.rsi_30, 
                 sp.stoch_fast_k_10_3, sp.stoch_fast_d_10_3,
                 sp.stoch_slow_k_10_3, sp.stoch_slow_d_10_3,
                 sp.stoch_fast_k_14_3, sp.stoch_fast_d_14_3,
                 sp.stoch_slow_k_14_3, sp.stoch_slow_d_14_3, sp.atr_14,
                 sp.vol_sma_5, sp.vol_sma_10, sp.vol_sma_20, sp.vol_sma_50,
                 sp.vol_sma_100, sp.vol_sma_200])
    
except csv.Error, e:
    print("A csv error occured: %s" % (e))    