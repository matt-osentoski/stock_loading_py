'''
Created on Jun 4, 2010

@author: Matt.Osentoski
'''
import urllib2
import csv
from lxml import etree

from model.stock import Stock

class IndexTools:
    '''
    This class is used to retrieve individual stock information
    from a stock index.
    '''

    YAHOO_URL = "http://download.finance.yahoo.com"
    SCRAPE_URL = "http://finance.yahoo.com/q/pr?s="
    ENTRIES_PER_PAGE = 50

    def get_index_array(self, index_symbol, index_name, num_stocks):
        end_loop = num_stocks / IndexTools.ENTRIES_PER_PAGE

        stocks = []
        try:
            for i in range(0, end_loop + 1):
                page = i * IndexTools.ENTRIES_PER_PAGE
                url = "%s/d/quotes.csv?s=%s&f=sl1d1t1c1ohgv&e=.csv&h=%d" % \
                    (IndexTools.YAHOO_URL, index_symbol, page)
                response = urllib2.urlopen(url)
                for row in csv.reader(response.readlines()):
                    if (row != None and len(row) > 0):
                        stock = Stock()
                        stock.symbol = row[0]
                        stock.stock_index = index_name
                        stocks.append(stock)
        except urllib2.URLError, url_e:
            print("A urllib2 error occured: %s" % (url_e))
        except csv.Error, e:
            print("A csv error occured: %s" % (e))
        return stocks  
    
    
    def scrape_stock_info(self, stock, verbose = 0):
        scrape_url = IndexTools.SCRAPE_URL + stock.symbol
        name = ''
        description = ''
        sector = ''
        industry = ''
        if (verbose != 0):
            print "Processing: %s" % (scrape_url)
        try:
            etree_html = etree.HTML(urllib2.urlopen(scrape_url).read())
            name = etree_html.xpath("string(//td[@class='yfnc_modtitlew2']/b)")
            description = etree_html.xpath("string(//td[@class='yfnc_modtitlew2']/table[4]/tr/td)")
            sector = etree_html.xpath("string(//td[@class='yfnc_modtitlew2']/table[2]/tr/td/table/tr[2]/td[2]/a)")
            industry = etree_html.xpath("string(//td[@class='yfnc_modtitlew2']/table[2]/tr/td/table/tr[3]/td[2]/a)")
        except urllib2.URLError, url_e:
            print("A urllib2 error occured: %s" % (url_e))
        except etree.LxmlError, lxml_e:
            print("An lxml error occured: %s" % (lxml_e))
        # Verify that the descriptors are formatted in UTF-8
        try:
            name = name.encode('utf-8')
            description = description.encode('utf-8')
            sector = sector.encode('utf-8')
            industry = industry.encode('utf-8')
        except UnicodeEncodeError, uni_e:
            print("An UnicodeEncodeError error occured: %s" % (uni_e))
        stock.name = name
        stock.description = description
        stock.sector = sector
        stock.industry = industry
        return stock