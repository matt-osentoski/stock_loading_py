'''
Created on Sep 12, 2010

@author: Matt Osentoski
This script loads historical treasury rates from:
http://www.federalreserve.gov
'''
import urllib2
import csv

from dao.treasury_dao import TreasuryDAO
from model.treasury import Treasury

def clean_cell(val):
    '''This function checks to see if the incoming cell contains "ND" or an
    empty space, and replaces it with a zero'''
    if (val == 'ND' or val == ''):
        return 0
    return val


TREASURY_URL = 'http://www.federalreserve.gov/datadownload/Output.aspx?rel=H15&series=bf17364827e38702b42a58cf8eaa3f78&lastObs=&from=&to=&filetype=csv&label=include&layout=seriescolumn&type=package'

treasuries = []

# First, truncate the treasuries table
print("Truncating the treasuries table...")
dao = TreasuryDAO()
dao.truncate_treasuries_table()
print("Truncate completed")

print("Adding treasury entries...")
try:
    response = urllib2.urlopen(TREASURY_URL)
    for row in csv.reader(response.readlines()):
        if (row != None and len(row) > 0):
            if (row[0].startswith('Series') or row[0].startswith('Unit') or
                row[0].startswith('Multiplier') or row[0].startswith('Currency') or
                row[0].startswith('Unique') or row[0].startswith('Time') ):
                continue
            t = Treasury()
            t.time_period = clean_cell(row[0])
            t.rate_1_month = clean_cell(row[1])
            t.rate_3_month = clean_cell(row[2])
            t.rate_6_month = clean_cell(row[3])
            t.rate_1_year = clean_cell(row[4])
            t.rate_2_year = clean_cell(row[5])
            t.rate_3_year = clean_cell(row[6])
            t.rate_5_year = clean_cell(row[7])
            t.rate_7_year = clean_cell(row[8])
            t.rate_10_year = clean_cell(row[9])
            t.rate_20_year = clean_cell(row[10])
            t.rate_30_year = clean_cell(row[11])
            treasuries.append(t)
except urllib2.URLError, url_e:
    print("A urllib2 error occured: %s" % (url_e))
except csv.Error, e:
    print("A csv error occured: %s" % (e))
    
# Persist the new records
for treasury in treasuries:
    dao.create_treasury(treasury)

print("Export completed: %s rows added" % len(treasuries))