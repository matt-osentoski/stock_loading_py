'''
Created on Jun 6, 2010

@author: Matt Osentoski

This script is used to load stock information into the database.

Set the 'end_loop' values
(~ lines 24  and 27) to the last page size for the components of the Nasdaq and
NYSE.  (For example: http://finance.yahoo.com/q/cp?s=^IXIC+Components
shows 3937 pages at 50 entries per page.  Round this value up to the nearest 50,
so the value for 'end_loop' will be '3950' for the Nasdaq.
'''

from dao.stock_dao import StockDAO
from utils.index_tools import IndexTools

INDICES = ['Nasdaq', 'NYSE']
LOG_FILE = '/temp/stock_importer.log'

f = open(LOG_FILE, 'w') # Used for logging this process
for index in INDICES:
    print("Processing %s" % index)
    f.write("Processing %s\n" % index)
    end_loop = 0
    index_symbol = ''
    
    if index == 'Nasdaq':
        index_symbol = "@%5EIXIC"  # NASDAQ Components
        end_loop = 3950
    elif index == 'NYSE':
        index_symbol = "@%5ENYA"  # NYSE Components (New Method)
        end_loop = 1850

    print "### First pass ###"
    f.write("### First pass ###\n")
    
    print "Gathering stocks from Yahoo"
    f.write("Gathering stocks from Yahoo\n")
    index_tools = IndexTools()
    stocks = index_tools.get_index_array(index_symbol, index, end_loop)
  
    print "Removing duplicates..."
    f.write("Removing duplicates...\n")
    tmp_set = set(stocks)
    stocks = list(tmp_set)

    print "Comparing with stocks currently in the database..."
    f.write("Comparing with stocks currently in the database...\n")
    # Grab all active stocks for a particular index
    dao = StockDAO()
    orig_stocks = dao.get_all_stocks_by_index(index, 1) # '1' for Active stocks only.
    all_stocks = dao.get_all_stocks()
    # Perform a compare against the stock arrays (using the '-' operator on the arrays)
    tmp_stocks_to_disable = set(orig_stocks) - set(stocks)
    stocks_to_disable = list(tmp_stocks_to_disable)
    print("stocks_to_disable: %s" % len(stocks_to_disable))
    f.write("stocks_to_disable: %s\n" % len(stocks_to_disable))
    for dis_stock in stocks_to_disable:
        print(dis_stock.symbol)
    tmp_stocks_to_add = set(stocks) - set(all_stocks)
    stocks_to_add = list(tmp_stocks_to_add)
    print("stocks_to_add: %s" % len(stocks_to_add))
    f.write("stocks_to_add: %s\n" % len(stocks_to_add))
    for add_stock in stocks_to_add:
        print(add_stock.symbol)
        f.write(add_stock.symbol + "\n")
    
    for dis_stock in stocks_to_disable:
        dao.mark_stock_inactive(dis_stock.symbol)
    for add_stock in stocks_to_add:
        dao.create_stock(add_stock)
  
    print("### Second Pass ###")
    print("Adding sector / industry information to the stocks...")
    f.write("### Second Pass ###\n")
    f.write("Adding sector / industry information to the stocks...\n")
  
    for stock in stocks_to_add:
        stock = index_tools.scrape_stock_info(stock)
        dao.update_stock_info(stock)
  
    print("### Third Pass ###")
    print("Updating stocks with index information...")
    f.write("### Third Pass ###\n")
    f.write("Updating stocks with index information...\n")
  
    print("Building index arrays")
    f.write("Building index arrays\n")
    sp500_stocks = index_tools.get_index_array("@%5EGSPC", "SP500", 500)
    sp1500_stocks = index_tools.get_index_array("@%5ESPSUPX", "SP1500", 1500)
    dow30_stocks = index_tools.get_index_array("@%5EDJI", "Dow30", 50)
    # These object arrays need to be converted to sets for scanning
    sp500_set = set(sp500_stocks)
    sp1500_set = set(sp1500_stocks)
    dow30_set = set(dow30_stocks)
  
    for stock in stocks_to_add:
        stock.is_sp500 = 0
        stock.is_sp1500 = 0
        stock.is_dow_ind = 0
        if stock in sp500_set:
            stock.is_sp500 = 1
        if stock in sp1500_set:
            stock.is_sp1500 = 1
        if stock in dow30_set:
            stock.is_dow_ind = 1
        dao.update_stock_indices(stock)
    
    print("%s has completed..." % (index))
    f.write("%s has completed...\n" % (index))

print("Export completed")
f.write("Export completed\n")
f.close()