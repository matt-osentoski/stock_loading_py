'''
Created on Jun 7, 2010

@author: Matt.Osentoski
This script loads historical data from Yahoo
'''
import urllib2
import csv
from datetime import datetime

from dao.stock_dao import StockDAO
from model.stock_price import StockPrice

YAHOO_URL = 'http://ichart.finance.yahoo.com'
PERIOD_ARR = ['d', 'w', 'm']
START_DATE = '1980-01-01'
UPDATED_START_DATE = '2010-04-10'
END_DATE = '2010-12-31'
LOG_FILE = '/temp/yahoo_data.log'

f = open(LOG_FILE, 'w') # Used for logging this process
dao = StockDAO()
  
# First gather the symbols from the database
stocks = []

stocks = dao.get_unprocessed_stocks(20000)

for stock in stocks:
    if stock.state == 'n':  # New stock
        start_date = datetime.strptime(START_DATE, '%Y-%m-%d')
        end_date = datetime.strptime(END_DATE, '%Y-%m-%d')
    elif stock.state == 'a': # A Stock that's available for update
        start_date = datetime.strptime(UPDATED_START_DATE, '%Y-%m-%d')
        end_date = datetime.strptime(END_DATE, '%Y-%m-%d')
  
    dao.update_stock_state(stock.symbol, 'p')
    print stock.symbol
    f.write(stock.symbol + "\n")
    for period in PERIOD_ARR:    
        try:
            url = "%s/table.csv?s=%s&a=%s&b=%s&c=%s&d=%s&e=%s&f=%s&g=%s&ignore=.csv" \
                % (YAHOO_URL, stock.symbol, start_date.month - 1,  
                   start_date.day, start_date.year, end_date.month - 1,
                   end_date.day, end_date.year, period)
            response = urllib2.urlopen(url)
            for x, row in enumerate(csv.reader(response.readlines())):
                if (x > 0 and row != None and len(row) > 0): # x > 0..skip the first row
                    sp = StockPrice()
                    sp.symbol = stock.symbol
                    sp.price_date = row[0]
                    sp.opening_price = row[1]
                    sp.high_price = row[2]
                    sp.low_price = row[3]
                    sp.closing_price = row[4]
                    sp.volume = row[5]
                    sp.adj_closing_price = row[6]
                    sp.period = period
                    dao.create_stock_price(sp)
        except urllib2.URLError, url_e:
            print("A urllib2 error occured: %s" % (url_e))
            f.write("A urllib2 error occured: %s\n" % (url_e))
        except csv.Error, e:
            print("A csv error occured: %s" % (e)) 
            f.write("A csv error occured: %s\n" % (e))
        print "Added: %s, %d entries for period %s" % (stock.symbol, x, period)
        f.write("Added: %s, %d entries for period %s\n" % (stock.symbol, x, period))
        
    dao.update_stock_state(stock.symbol, 'f')
f.close()
