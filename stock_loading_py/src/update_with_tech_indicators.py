from dao.stock_dao import StockDAO
from technicals.technical_indicators import TechnicalIndicators
from time import strftime
import time
import sys
import psyco

psyco.full()

PERIODS = ['d', 'w', 'm']

print "Start Time: ", strftime("%a, %d %b %Y %H:%M:%S", time.localtime(time.time()))

dao = StockDAO()

# Get all stocks
stocks = dao.get_all_stocks()

# Loop through the stocks
for stock_idx, stock in enumerate(stocks):

    print "Processing ", stock.symbol

    # For testing a single stock, uncomment this line.
    #
    if (stock_idx > 0):
        sys.exit(0)
  
    # Loop by period
    for period in PERIODS:
        #Below are variable that hold the previous EMA values
        prev_ema_5 = 0
        prev_ema_10 = 0
        prev_ema_20 = 0
        prev_ema_50 = 0
        prev_ema_100 = 0
        prev_ema_200 = 0
        prev_ema_12 = 0
        prev_ema_26 = 0
        prev_macd_12_26_signal_9 = 0
        prev_ppo_12_26_signal_9 = 0
        prev_atr_14 = 0
    
        # Get all stock prices
        tmpStock = dao.get_all_historic_stock_prices(stock.symbol, period)
        stock_prices = tmpStock.prices
        for price_idx, stock_price in enumerate(stock_prices):
    
            #TODO Apply tech rules
            ti = TechnicalIndicators()
            # SMA's
            stock_price.sma_5 = ti.get_sma(stock_prices, price_idx, 5)
            stock_price.sma_10 = ti.get_sma(stock_prices, price_idx, 10)
            stock_price.sma_20 = ti.get_sma(stock_prices, price_idx, 20)
            stock_price.sma_50 = ti.get_sma(stock_prices, price_idx, 50)
            stock_price.sma_100 = ti.get_sma(stock_prices, price_idx, 100)
            stock_price.sma_200 = ti.get_sma(stock_prices, price_idx, 200)
          
            # EMA's
            stock_price.ema_5 = ti.get_ema(stock_prices, price_idx, 5, prev_ema_5)
            prev_ema_5 = stock_price.ema_5
            stock_price.ema_10 = ti.get_ema(stock_prices, price_idx, 10, prev_ema_10)
            prev_ema_10 = stock_price.ema_10
            stock_price.ema_20 = ti.get_ema(stock_prices, price_idx, 20, prev_ema_20)
            prev_ema_20 = stock_price.ema_20
            stock_price.ema_50 = ti.get_ema(stock_prices, price_idx, 50, prev_ema_50)
            prev_ema_50 = stock_price.ema_50
            stock_price.ema_100 = ti.get_ema(stock_prices, price_idx, 100, prev_ema_100)
            prev_ema_100 = stock_price.ema_100
            stock_price.ema_200 = ti.get_ema(stock_prices, price_idx, 200, prev_ema_200)
            prev_ema_200 = stock_price.ema_200
    
            # MACD
            stock_price.ema_12 = ti.get_ema(stock_prices, price_idx, 12, prev_ema_12)
            prev_ema_12 = stock_price.ema_12
            stock_price.ema_26 = ti.get_ema(stock_prices, price_idx, 26, prev_ema_26)
            prev_ema_26 = stock_price.ema_26
            if (price_idx + 1) >= 26:
                stock_price.macd_12_26 = ti.get_macd(stock_price.ema_12, stock_price.ema_26)
            else:
                stock_price.macd_12_26 = 0
                stock_price.macd_12_26_signal_9 = 0
                
            if (price_idx + 1) >= (26 + 9):
                stock_price.macd_12_26_signal_9 = ti.get_macd_ema(stock_prices, price_idx, 9, prev_macd_12_26_signal_9)    
                prev_macd_12_26_signal_9 = stock_price.macd_12_26_signal_9
                stock_price.macd_hist_12_26 = ti.get_macd_histogram(stock_price.macd_12_26, stock_price.macd_12_26_signal_9)
            else:
                stock_price.macd_12_26_signal_9 = 0
                stock_price.macd_hist_12_26 = 0
                
            # PPO
            if (price_idx + 1) >= 26:
                stock_price.ppo_12_26 = ti.get_ppo(stock_price.ema_12, stock_price.ema_26)      
            else:
                stock_price.ppo_12_26 = 0
                stock_price.ppo_12_26_signal_9 = 0
                stock_price.ppo_hist_12_26 = 0

      
            if (price_idx + 1) >= (26 + 9):
                stock_price.ppo_12_26_signal_9 = ti.get_ppo_ema(stock_prices, price_idx, 9, prev_ppo_12_26_signal_9)
                prev_ppo_12_26_signal_9 = stock_price.ppo_12_26_signal_9
                stock_price.ppo_hist_12_26 = ti.get_ppo_histogram(stock_price.ppo_12_26, stock_price.ppo_12_26_signal_9)
            else:
                stock_price.ppo_12_26_signal_9 = 0
                stock_price.ppo_hist_12_26 = 0
                
            # RSI
            stock_price.rsi_14 = ti.get_rsi(stock_prices, price_idx, 14)
            stock_price.rsi_20 = ti.get_rsi(stock_prices, price_idx, 20)
            stock_price.rsi_30 = ti.get_rsi(stock_prices, price_idx, 30)
          
            # Stochastics
            stock_price.stoch_fast_k_10_3, stock_price.stoch_fast_d_10_3, stock_price.stoch_slow_k_10_3, stock_price.stoch_slow_d_10_3 = ti.get_stochastics(stock_prices, price_idx)
            stock_price.stoch_fast_k_14_3, stock_price.stoch_fast_d_14_3, stock_price.stoch_slow_k_14_3, stock_price.stoch_slow_d_14_3 = ti.get_stochastics(stock_prices, price_idx, 14, 3)
            
            # Volume SMA
            stock_price.vol_sma_5 = ti.get_vol_sma(stock_prices, price_idx, 5)
            stock_price.vol_sma_10 = ti.get_vol_sma(stock_prices, price_idx, 10)
            stock_price.vol_sma_20 = ti.get_vol_sma(stock_prices, price_idx, 20)
            stock_price.vol_sma_50 = ti.get_vol_sma(stock_prices, price_idx, 50)
            stock_price.vol_sma_100 = ti.get_vol_sma(stock_prices, price_idx, 100)
            stock_price.vol_sma_200 = ti.get_vol_sma(stock_prices, price_idx, 200)
    
            # ATR 14
            stock_price.atr_14 = ti.get_atr(stock_prices, price_idx, prev_atr_14, 14)
            prev_atr_14 = stock_price.atr_14
            
            # Historical Volatility (Simple) 20 day period, 252 trading days in a year
            stock_price.hist_vola_20_252 = ti.get_historical_volatility(stock_prices, price_idx, 20, 252)
    
            # Update the stock_price  
            dao.update_stock_price_technicals(stock_price)

print "End Time: ", strftime("%a, %d %b %Y %H:%M:%S", time.localtime(time.time()))
