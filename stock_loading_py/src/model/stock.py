class Stock:
    
    def __hash__(self):
        return hash(self.symbol)
    def __eq__(self, other):
        if isinstance(other, Stock):
            return self.symbol == other.symbol
        return NotImplemented
    def __ne__(self, other):
        result = self.__eq__(other)
        if result is NotImplemented:
            return result
        return not result

    
    id = 0
    symbol = ""
    name = ""
    description = "" 
    sector = "" 
    industry = "" 
    stock_index = ""
    active = 0
    state = ""
    is_sp500 = 0 
    is_sp1500 = 0 
    is_dow_ind = 0
    prices = []