'''
Created on Sep 12, 2010

@author: Matt
'''

class Treasury:
    '''
    This class represents different treasury rates on a particular day
    '''
    id = 0
    time_period = 0
    rate_1_month = 0
    rate_3_month = 0
    rate_6_month = 0
    rate_1_year = 0
    rate_2_year = 0
    rate_3_year = 0
    rate_5_year = 0
    rate_7_year = 0
    rate_10_year = 0
    rate_20_year = 0
    rate_30_year = 0
        