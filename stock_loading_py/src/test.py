from model.stock_price import StockPrice
from model.stock import Stock
from dao.stock_dao import StockDAO
from technicals.binomial_formulas import *
from technicals.technical_indicators import TechnicalIndicators

ti = TechnicalIndicators()
#close_arr = [101.0, 102.0, 103.0, 102.0, 101.0, 100.0, 101.0, 102.0, 103.0, 102.0, 101.0, 100.0, 101.0, 102.0, 103.0, 102.0, 101.0, 100.0, 102.0, 103.0]
#close_arr = [100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0]
#close_arr = [100.0, 105.0, 102.30, 101.10, 104.82, 106.01, 106.0, 105.25, 107.10, 106.44, 108.02, 107.86]
close_arr = [11.73, 11.87, 11.6, 11.28, 11.23, 11.1, 10.92, 10.67, 10.66, 10.61, 10.69, 10.71, 10.58, 10.32]
strike = 12.0
volatility = 0.20 # 20% volatility
risk_free_rate = 0.09 # 3 month treasury bill
STEPS = 500
daily_time_decay = 0.004 #  1/250 (250 trading days in a year)

start_call = 0.0
end_call = 0.0

t = 0.25 # 3 months.        
for x, price in enumerate(close_arr):
    S = price 
    K = strike
    r = risk_free_rate
    sigma = volatility
    # Each day's time value should be reduced by the daily time decay.  Linear, in this simple test
    if (x > 0):
        t = t - daily_time_decay  
    steps = STEPS
    option_price_call = option_price_call_american_binomial(S, K, r, sigma, t, steps)
    print("Call %s" % (option_price_call))
    if (x == 0):
        start_call = option_price_call
    if (x == len(close_arr)-1):
        end_call = option_price_call
print("Call P&L %s" % (end_call - start_call))
       

start_put = 0.0
end_put = 0.0
 
t = 0.25   
for x, price in enumerate(close_arr):
    S = price 
    K = strike
    r = risk_free_rate
    sigma = volatility
    if (x > 0):
        t = t - daily_time_decay  
    steps = STEPS
    option_price_put = option_price_put_american_binomial(S, K, r, sigma, t, steps)
    print("Put %s" % (option_price_put))
    if (x == 0):
        start_put = option_price_put
    if (x == len(close_arr)-1):
        end_put = option_price_put
print("Put P&L %s" % (end_put - start_put))

profit_loss = ((end_call - start_call) + (end_put - start_put)) * 100

print("Total P&L %s" % ( profit_loss ))