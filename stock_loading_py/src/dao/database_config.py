'''
Created on Sep 12, 2010

@author: Matt Osentoski
'''

class DatabaseConfig:
    '''
    This class holds the database configuration
    '''
    
    DB_HOST = "localhost"
    DB_USERNAME = 'root'
    DB_PASSWORD = 'password'
    DB_DATABASE = 'stocks_12312010'

        