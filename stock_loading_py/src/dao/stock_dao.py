import MySQLdb
import sqlalchemy.pool as pool

import dao.dbpool as dbpool
from model.stock import Stock
from model.stock_price import StockPrice
from dao.database_config import DatabaseConfig

class StockDAO:

    
    def get_connection(self):
        return dbpool.mysql.connect(host=DatabaseConfig.DB_HOST, user=DatabaseConfig.DB_USERNAME,
                               passwd=DatabaseConfig.DB_PASSWORD, 
                               db=DatabaseConfig.DB_DATABASE)
        
    def get_random_stock_symbol(self):
        symbol = ""
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = "SELECT symbol FROM stocks s ORDER BY RAND() LIMIT 1"
            cursor.execute(sql)
            rs = cursor.fetchall()
            for row in rs:
                symbol = row[0]
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_random_stock_symbol: Error %d: %s" % (
                e.args[0], e.args[1])
        return symbol
    
    def get_stock(self, symbol):
        stock = Stock()
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, name, description, sector, 
                industry, stock_index, state, active 
                FROM stocks WHERE symbol = %s"""
            cursor.execute(sql, (symbol))
            rs = cursor.fetchall()
            for row in rs:
                stock.id = row[0]
                stock.symbol = row[1]
                stock.name = row[2]
                stock.description = row[3]
                stock.sector = row[4]
                stock.industry = row[5]
                stock.stock_index = row[6]
                stock.state = row[7]
                stock.active = row[8]
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_stock: Error %d: %s" % (e.args[0], e.args[1])
        return stock
        
    def get_all_stocks(self): 
        stocks = []
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, name, description, sector, 
                industry, stock_index, state, active FROM stocks"""
            cursor.execute(sql)
            rs = cursor.fetchall()
            for row in rs:
                stock = Stock()
                stock.id = row[0]
                stock.symbol = row[1]
                stock.name = row[2]
                stock.description = row[3]
                stock.sector = row[4]
                stock.industry = row[5]
                stock.stock_index = row[6]
                stock.state = row[7]
                stock.active = row[8]
                stocks.append(stock)
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_all_stocks: Error %d: %s" % (e.args[0], e.args[1])
        return stocks    
    
    def get_all_active_stocks(self): 
        stocks = []
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, name, description, sector, 
                industry, stock_index, state, active FROM stocks
                WHERE active = 1"""
            cursor.execute(sql)
            rs = cursor.fetchall()
            for row in rs:
                stock = Stock()
                stock.id = row[0]
                stock.symbol = row[1]
                stock.name = row[2]
                stock.description = row[3]
                stock.sector = row[4]
                stock.industry = row[5]
                stock.stock_index = row[6]
                stock.state = row[7]
                stock.active = row[8]
                stocks.append(stock)
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_all_active_stocks: Error %d: %s" % (
                e.args[0], e.args[1])
        return stocks
    
    def get_all_stocks_by_index(self, index, active=1): 
        stocks = []
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, name, description, sector, 
                industry, stock_index, state, active FROM stocks
                WHERE stock_index = %s AND active = %s"""
            cursor.execute(sql, (index,active))
            rs = cursor.fetchall()
            for row in rs:
                stock = Stock()
                stock.id = row[0]
                stock.symbol = row[1]
                stock.name = row[2]
                stock.description = row[3]
                stock.sector = row[4]
                stock.industry = row[5]
                stock.stock_index = row[6]
                stock.state = row[7]
                stock.active = row[8]
                stocks.append(stock)
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_all_stocks_by_index: Error %d: %s" % (
                e.args[0], e.args[1])
        return stocks
    
    def get_unprocessed_stock(self): 
        stocks = []
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, name, description, sector, 
                industry, stock_index, state, active FROM stocks
                WHERE (state = 'a' OR state = 'n') AND active = 1 LIMIT 1"""
            cursor.execute(sql)
            rs = cursor.fetchall()
            # Normally the stock object instantiation would go in the for
            # loop, but we're limiting it to 1 stock and need access to the 
            # object outside the loop.
            stock = Stock()
            for row in rs: 
                stock.id = row[0]
                stock.symbol = row[1]
                stock.name = row[2]
                stock.description = row[3]
                stock.sector = row[4]
                stock.industry = row[5]
                stock.stock_index = row[6]
                stock.state = row[7]
                stock.active = row[8]
                stocks.append(stock)
            self.update_stock_state(stock.symbol, "p")
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_unprocessed_stock: Error %d: %s" % (
                e.args[0], e.args[1])
        return stocks
    
    def get_unprocessed_stocks(self, count=10000): 
        stocks = []
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, name, description, sector, 
                industry, stock_index, state, active FROM stocks 
                WHERE (state = 'a' OR state = 'n') AND active = '1' LIMIT %s"""
            cursor.execute(sql, (count))
            rs = cursor.fetchall()
            for row in rs:
                stock = Stock()
                stock.id = row[0]
                stock.symbol = row[1]
                stock.name = row[2]
                stock.description = row[3]
                stock.sector = row[4]
                stock.industry = row[5]
                stock.stock_index = row[6]
                stock.state = row[7]
                stock.active = row[8]
                stocks.append(stock)
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_unprocessed_stocks: Error %d: %s" % (
                e.args[0], e.args[1])
        return stocks
    
    def get_all_historic_stock_prices(self, symbol, period):
        """Returns a Stock object loaded with historical prices"""
        
        # First, get the stock object based on the symbol 
        stock = self.get_stock(symbol)
        stock.prices = []
        if (stock == None):
            return
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, opening_price, closing_price, 
                adj_closing_price,
                low_price, high_price, volume, price_date, sma_5,
                sma_10, sma_20, sma_50, sma_100, sma_200,
                ema_5, ema_10, ema_20, ema_50, ema_100, ema_200, 
                ema_12, ema_26, macd_12_26, macd_12_26_signal_9, 
                macd_hist_12_26, ppo_12_26, ppo_12_26_signal_9, 
                ppo_hist_12_26, rsi_14, rsi_20, rsi_30,
                stoch_fast_k_10_3, stoch_fast_d_10_3,
                stoch_slow_k_10_3, stoch_slow_d_10_3,
                stoch_fast_k_14_3, stoch_fast_d_14_3,
                stoch_slow_k_14_3, stoch_slow_d_14_3,
                vol_sma_5, vol_sma_10, vol_sma_20, vol_sma_50,
                vol_sma_100, vol_sma_200, atr_14, hist_vola_20_252
                FROM stock_prices WHERE stock_id = %s AND period = %s 
                ORDER BY price_date"""
            cursor.execute(sql, (stock.id, period))
            rs = cursor.fetchall()
            for row in rs:
                stock_price = StockPrice()
                stock_price.id = row[0]
                stock_price.symbol = row[1]
                stock_price.opening_price = row[2]
                stock_price.closing_price = row[3]
                stock_price.adj_closing_price = row[4]
                stock_price.low_price = row[5]
                stock_price.high_price = row[6]
                stock_price.volume = row[7]
                stock_price.price_date = row[8]
                stock_price.sma_5 = row[9]
                stock_price.sma_10 = row[10] 
                stock_price.sma_20 = row[11] 
                stock_price.sma_50 = row[12] 
                stock_price.sma_100 = row[13] 
                stock_price.sma_200 = row[14]
                stock_price.ema_5 = row[15] 
                stock_price.ema_10 = row[16] 
                stock_price.ema_20 = row[17] 
                stock_price.ema_50 = row[18] 
                stock_price.ema_100 = row[19] 
                stock_price.ema_200 = row[20] 
                stock_price.ema_12 = row[21] 
                stock_price.ema_26 = row[22] 
                stock_price.macd_12_26 = row[23] 
                stock_price.macd_12_26_signal_9 = row[24]
                stock_price.macd_hist_12_26 = row[25] 
                stock_price.ppo_12_26 = row[26] 
                stock_price.ppo_12_26_signal_9 = row[27] 
                stock_price.ppo_hist_12_26 = row[28] 
                stock_price.rsi_14 = row[29] 
                stock_price.rsi_20 = row[30] 
                stock_price.rsi_30 = row[31]
                stock_price.stoch_fast_k_10_3 = row[32] 
                stock_price.stoch_fast_d_10_3 = row[33]
                stock_price.stoch_slow_k_10_3 = row[34] 
                stock_price.stoch_slow_d_10_3 = row[35]
                stock_price.stoch_fast_k_14_3 = row[36] 
                stock_price.stoch_fast_d_14_3 = row[37]
                stock_price.stoch_slow_k_14_3 = row[38] 
                stock_price.stoch_slow_d_14_3 = row[39]
                stock_price.vol_sma_5 = row[40] 
                stock_price.vol_sma_10 = row[41] 
                stock_price.vol_sma_20 = row[42] 
                stock_price.vol_sma_50 = row[43]
                stock_price.vol_sma_100 = row[44] 
                stock_price.vol_sma_200 = row[45] 
                stock_price.atr_14 = row[46]
                stock_price.hist_vola_20_252 = row[47]
                
                stock.prices.append(stock_price)
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "get_all_historic_stock_prices: Error %d: %s" % (
                e.args[0], e.args[1])
        return stock
  
    def get_historic_stock_prices(self, symbol, period, start_date, size):
        """Returns a Stock object loaded with historical prices"""
        
        # First, get the stock object based on the symbol 
        stock = self.get_stock(symbol)
        stock.prices = []
        if (stock == None):
            return
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, opening_price, closing_price, 
                adj_closing_price,
                low_price, high_price, volume, price_date, sma_5,
                sma_10, sma_20, sma_50, sma_100, sma_200,
                ema_5, ema_10, ema_20, ema_50, ema_100, ema_200, 
                ema_12, ema_26, macd_12_26, macd_12_26_signal_9, 
                macd_hist_12_26, 
                ppo_12_26, ppo_12_26_signal_9, ppo_hist_12_26, 
                rsi_14, rsi_20, rsi_30,
                stoch_fast_k_10_3, stoch_fast_d_10_3,
                stoch_slow_k_10_3, stoch_slow_d_10_3,
                stoch_fast_k_14_3, stoch_fast_d_14_3,
                stoch_slow_k_14_3, stoch_slow_d_14_3,
                vol_sma_5, vol_sma_10, vol_sma_20, vol_sma_50,
                vol_sma_100, vol_sma_200, atr_14, hist_vola_20_252 
                FROM stock_prices WHERE stock_id = %s AND price_date > %s 
                AND period = %s ORDER BY price_date LIMIT %s"""
            cursor.execute(sql, (stock.id, start_date, period, size))
            rs = cursor.fetchall()
            for row in rs:
                stock_price = StockPrice()
                stock_price.id = row[0]
                stock_price.symbol = row[1]
                stock_price.opening_price = row[2]
                stock_price.closing_price = row[3]
                stock_price.adj_closing_price = row[4]
                stock_price.low_price = row[5]
                stock_price.high_price = row[6]
                stock_price.volume = row[7]
                stock_price.price_date = row[8]
                stock_price.sma_5 = row[9]
                stock_price.sma_10 = row[10] 
                stock_price.sma_20 = row[11] 
                stock_price.sma_50 = row[12] 
                stock_price.sma_100 = row[13] 
                stock_price.sma_200 = row[14]
                stock_price.ema_5 = row[15] 
                stock_price.ema_10 = row[16] 
                stock_price.ema_20 = row[17] 
                stock_price.ema_50 = row[18] 
                stock_price.ema_100 = row[19] 
                stock_price.ema_200 = row[20] 
                stock_price.ema_12 = row[21] 
                stock_price.ema_26 = row[22] 
                stock_price.macd_12_26 = row[23] 
                stock_price.macd_12_26_signal_9 = row[24]
                stock_price.macd_hist_12_26 = row[25] 
                stock_price.ppo_12_26 = row[26] 
                stock_price.ppo_12_26_signal_9 = row[27] 
                stock_price.ppo_hist_12_26 = row[28] 
                stock_price.rsi_14 = row[29] 
                stock_price.rsi_20 = row[30] 
                stock_price.rsi_30 = row[31]
                stock_price.stoch_fast_k_10_3 = row[32] 
                stock_price.stoch_fast_d_10_3 = row[33]
                stock_price.stoch_slow_k_10_3 = row[34] 
                stock_price.stoch_slow_d_10_3 = row[35]
                stock_price.stoch_fast_k_14_3 = row[36] 
                stock_price.stoch_fast_d_14_3 = row[37]
                stock_price.stoch_slow_k_14_3 = row[38] 
                stock_price.stoch_slow_d_14_3 = row[39]
                stock_price.vol_sma_5 = row[40] 
                stock_price.vol_sma_10 = row[41] 
                stock_price.vol_sma_20 = row[42] 
                stock_price.vol_sma_50 = row[43]
                stock_price.vol_sma_100 = row[44] 
                stock_price.vol_sma_200 = row[45] 
                stock_price.atr_14 = row[46] 
                stock_price.hist_vola_20_252 = row[47]
                
                stock.prices.append(stock_price)
            cursor.close()
            conn.close()    
        except MySQLdb.Error, e:
            print "get_historic_stock_prices: Error %d: %s" % (
                e.args[0], e.args[1])
        return stock
    
    def update_stock_price_technicals(self, stock_price):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """UPDATE stock_prices SET sma_5 = %s, sma_10 = %s, 
                sma_20 = %s, sma_50 = %s, sma_100 = %s, sma_200 = %s, 
                ema_5 = %s, ema_10 = %s, ema_20 = %s, ema_50 = %s, 
                ema_100 = %s, ema_200 = %s, ema_12 = %s, ema_26 = %s, 
                macd_12_26 = %s, macd_12_26_signal_9 = %s, 
                macd_hist_12_26 = %s, ppo_12_26 = %s, ppo_12_26_signal_9 = %s, 
                ppo_hist_12_26 = %s, rsi_14 = %s, rsi_20 = %s, rsi_30 = %s,
                stoch_fast_k_10_3 = %s, stoch_fast_d_10_3 = %s,
                stoch_slow_k_10_3 = %s, stoch_slow_d_10_3 = %s,
                stoch_fast_k_14_3 = %s, stoch_fast_d_14_3 = %s,
                stoch_slow_k_14_3 = %s, stoch_slow_d_14_3 = %s,
                vol_sma_5 = %s, vol_sma_10 = %s, vol_sma_20 = %s, 
                vol_sma_50 = %s, vol_sma_100 = %s, vol_sma_200 = %s, 
                atr_14 = %s, hist_vola_20_252 = %s WHERE id = %s"""
            cursor.execute(sql, (stock_price.sma_5, stock_price.sma_10, 
                                 stock_price.sma_20, stock_price.sma_50, 
                                 stock_price.sma_100, stock_price.sma_200,
                                 stock_price.ema_5, stock_price.ema_10, 
                                 stock_price.ema_20, stock_price.ema_50, 
                                 stock_price.ema_100, stock_price.ema_200, 
                                 stock_price.ema_12, stock_price.ema_26, 
                                 stock_price.macd_12_26, 
                                 stock_price.macd_12_26_signal_9, 
                                 stock_price.macd_hist_12_26, 
                                 stock_price.ppo_12_26, 
                                 stock_price.ppo_12_26_signal_9, 
                                 stock_price.ppo_hist_12_26, 
                                 stock_price.rsi_14, stock_price.rsi_20, 
                                 stock_price.rsi_30,
                                 stock_price.stoch_fast_k_10_3, 
                                 stock_price.stoch_fast_d_10_3,
                                 stock_price.stoch_slow_k_10_3, 
                                 stock_price.stoch_slow_d_10_3,
                                 stock_price.stoch_fast_k_14_3, 
                                 stock_price.stoch_fast_d_14_3,
                                 stock_price.stoch_slow_k_14_3, 
                                 stock_price.stoch_slow_d_14_3,
                                 stock_price.vol_sma_5, stock_price.vol_sma_10, 
                                 stock_price.vol_sma_20, 
                                 stock_price.vol_sma_50,
                                 stock_price.vol_sma_100, 
                                 stock_price.vol_sma_200, stock_price.atr_14,
                                 stock_price.hist_vola_20_252,
                                 stock_price.id ))
            cursor.close()
            conn.commit()
            conn.close()
        except MySQLdb.Error, e:
            print "update_stock_price_technicals: Error %d: %s" % (
                e.args[0], e.args[1])
        return
    
    def update_stock_state(self, symbol, state):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """UPDATE stocks SET state = %s WHERE symbol = %s"""
            cursor.execute(sql, (state, symbol ))
            cursor.close()
            conn.commit()
            
        except MySQLdb.Error, e:
            print "update_stock_state: Error %d: %s" % (e.args[0], e.args[1])
        return
    
    def mark_stock_inactive(self, symbol):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = "UPDATE stocks SET active = '0' WHERE symbol = %s"
            cursor.execute(sql, (symbol ))
            cursor.close()
            conn.commit()
            
        except MySQLdb.Error, e:
            print "mark_stock_inactive: Error %d: %s" % (e.args[0], e.args[1])
        return
    
    def update_stock_info(self, stock):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """UPDATE stocks SET name = %s, description = %s, 
                sector = %s, industry = %s WHERE symbol = %s"""
            cursor.execute(sql, (stock.name, stock.description, 
                                 stock.sector, stock.industry, 
                                 stock.symbol ))
            cursor.close()
            conn.commit()
            
        except MySQLdb.Error, e:
            print "update_stock_info: Error %d: %s" % (e.args[0], e.args[1])
        return
    
    def update_stock_indices(self, stock):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """UPDATE stocks SET is_sp500 = %s, 
                is_sp1500 = %s, is_dow_ind = %s WHERE symbol = %s"""
            cursor.execute(sql, (stock.is_sp500, stock.is_sp1500, 
                                 stock.is_dow_ind, stock.symbol ))
            cursor.close()
            conn.commit()
            
        except MySQLdb.Error, e:
            print "update_stock_indices: Error %d: %s" % (e.args[0], e.args[1])
        return
    
    def create_stock_price(self, stock_price):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """INSERT INTO stock_prices (symbol, price_date, 
                opening_price, high_price, low_price, closing_price, 
                volume, adj_closing_price, period) VALUES 
                (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
            cursor.execute(sql, (stock_price.symbol, 
                                 stock_price.price_date, 
                                 stock_price.opening_price, 
                                 stock_price.high_price,
                                 stock_price.low_price, 
                                 stock_price.closing_price, 
                                 stock_price.volume, 
                                 stock_price.adj_closing_price, 
                                 stock_price.period ))
            cursor.close()
            conn.commit()
            
        except MySQLdb.Error, e:
            print "create_stock_price: Error %d: %s" % (e.args[0], e.args[1])
        return
    
    def create_stock(self, stock):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """INSERT INTO stocks (symbol, name,
                description, sector, industry,
                stock_index) VALUES (%s, %s, %s, %s, %s, %s)"""
            cursor.execute(sql, (stock.symbol, stock.name, stock.description, 
                                 stock.sector, stock.industry,
                                 stock.stock_index ))
            cursor.close()
            conn.commit()
            
        except MySQLdb.Error, e:
            print "create_stock: Error %d: %s" % (e.args[0], e.args[1])
        return
    
    def export_stock_prices(self, start_date, end_date, 
                                index_col = "s.is_sp500"):
        """Returns a Stock for export into XML, CSV, JSON, etc.
           - start_date The starting date formatted as yyyy-MM-dd  
           - end_date The ending date formatted as yyyy-MM-dd
           - index_col The column in the database used to determine
             stocks that belong to a particular index (Ex: s.is_sp500)
        """

        stock_prices = []
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT sp.id, sp.symbol, sp.opening_price, 
                sp.closing_price, sp.adj_closing_price, sp.low_price, 
                sp.high_price, sp.volume, sp.price_date, sp.sma_5,
                sp.sma_10, sp.sma_20, sp.sma_50, sp.sma_100, sp.sma_200,
                sp.ema_5, sp.ema_10, sp.ema_20, sp.ema_50, sp.ema_100, 
                sp.ema_200, sp.ema_12, sp.ema_26, sp.macd_12_26, 
                sp.macd_12_26_signal_9, sp.macd_hist_12_26, sp.ppo_12_26, 
                sp.ppo_12_26_signal_9, sp.ppo_hist_12_26, sp.rsi_14, 
                sp.rsi_20, sp.rsi_30, sp.stoch_fast_k_10_3, 
                sp.stoch_fast_d_10_3, sp.stoch_slow_k_10_3, 
                sp.stoch_slow_d_10_3, sp.stoch_fast_k_14_3, 
                sp.stoch_fast_d_14_3, sp.stoch_slow_k_14_3, 
                sp.stoch_slow_d_14_3, sp.vol_sma_5, sp.vol_sma_10, 
                sp.vol_sma_20, sp.vol_sma_50, sp.vol_sma_100, 
                sp.vol_sma_200, sp.atr_14, sp.hist_vola_20_252, sp.period
                FROM stock_prices sp INNER JOIN stocks s
                ON s.symbol = sp.symbol
                WHERE """ + index_col + """ = '1' AND
                price_date >= %s AND 
                price_date <= %s ORDER BY 
                symbol, period, price_date""" 
            cursor.execute(sql, (start_date, end_date))
            rs = cursor.fetchall()
            for row in rs:
                stock_price = StockPrice()
                stock_price.id = row[0]
                stock_price.symbol = row[1]
                stock_price.opening_price = row[2]
                stock_price.closing_price = row[3]
                stock_price.adj_closing_price = row[4]
                stock_price.low_price = row[5]
                stock_price.high_price = row[6]
                stock_price.volume = row[7]
                stock_price.price_date = row[8]
                stock_price.sma_5 = row[9]
                stock_price.sma_10 = row[10] 
                stock_price.sma_20 = row[11] 
                stock_price.sma_50 = row[12] 
                stock_price.sma_100 = row[13] 
                stock_price.sma_200 = row[14]
                stock_price.ema_5 = row[15] 
                stock_price.ema_10 = row[16] 
                stock_price.ema_20 = row[17] 
                stock_price.ema_50 = row[18]
                stock_price.ema_100 = row[19] 
                stock_price.ema_200 = row[20] 
                stock_price.ema_12 = row[21]
                stock_price.ema_26 = row[22] 
                stock_price.macd_12_26 = row[23] 
                stock_price.macd_12_26_signal_9 = row[24]
                stock_price.macd_hist_12_26 = row[25] 
                stock_price.ppo_12_26 = row[26]
                stock_price.ppo_12_26_signal_9 = row[27] 
                stock_price.ppo_hist_12_26 = row[28] 
                stock_price.rsi_14 = row[29] 
                stock_price.rsi_20 = row[30]
                stock_price.rsi_30 = row[31]
                stock_price.stoch_fast_k_10_3 = row[32] 
                stock_price.stoch_fast_d_10_3 = row[33]
                stock_price.stoch_slow_k_10_3 = row[34] 
                stock_price.stoch_slow_d_10_3 = row[35]
                stock_price.stoch_fast_k_14_3 = row[36] 
                stock_price.stoch_fast_d_14_3 = row[37]
                stock_price.stoch_slow_k_14_3 = row[38] 
                stock_price.stoch_slow_d_14_3 = row[39]
                stock_price.vol_sma_5 = row[40] 
                stock_price.vol_sma_10 = row[41] 
                stock_price.vol_sma_20 = row[42] 
                stock_price.vol_sma_50 = row[43]
                stock_price.vol_sma_100 = row[44] 
                stock_price.vol_sma_200 = row[45] 
                stock_price.atr_14 = row[46]
                stock_price.hist_vola_20_252 = row[47]
                stock_price.period = row[48]
                
                stock_prices.append(stock_price)
            cursor.close()
            conn.commit()    
        except MySQLdb.Error, e:
            print "export_stock_prices: Error %d: %s" % (
                e.args[0], e.args[1])
        return stock_prices
    
    def export_stocks(self, index_col = "s.is_sp500"):
        stocks = []
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """SELECT id, symbol, name, description, sector, 
                industry, stock_index, state, active 
                FROM stocks WHERE """ + index_col + """ = '1' ORDER BY symbol"""
            cursor.execute(sql)
            rs = cursor.fetchall()
            for row in rs:
                stock = Stock()
                stock.id = row[0]
                stock.symbol = row[1]
                stock.name = row[2]
                stock.description = row[3]
                stock.sector = row[4]
                stock.industry = row[5]
                stock.stock_index = row[6]
                stock.state = row[7]
                stock.active = row[8]
                stocks.append(stock)
            cursor.close()
            conn.commit()
        except MySQLdb.Error, e:
            print "get_stock: Error %d: %s" % (e.args[0], e.args[1])
        return stocks