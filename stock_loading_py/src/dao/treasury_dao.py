'''
Created on Sep 12, 2010

@author: Matt Osentoski
'''
import MySQLdb
import sqlalchemy.pool as pool

import dao.dbpool as dbpool
from dao.database_config import DatabaseConfig

class TreasuryDAO:
    '''
    Database operations for treasuries
    '''

    def get_connection(self):
        return dbpool.mysql.connect(host=DatabaseConfig.DB_HOST, user=DatabaseConfig.DB_USERNAME,
                               passwd=DatabaseConfig.DB_PASSWORD, 
                               db=DatabaseConfig.DB_DATABASE)

    def truncate_treasuries_table(self):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = "TRUNCATE TABLE treasuries"
            cursor.execute(sql)
            cursor.close()
            conn.close()
        except MySQLdb.Error, e:
            print "truncate_treasuries_table: Error %d: %s" % (
                e.args[0], e.args[1])

    def create_treasury(self, treasury):
        try:
            conn = self.get_connection()
            cursor = conn.cursor()
            sql = """INSERT INTO treasuries (time_period,
                1_month, 3_month, 6_month, 1_year, 2_year, 3_year, 5_year,
                7_year, 10_year, 20_year, 30_year) VALUES 
                (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
            cursor.execute(sql, (treasury.time_period, 
                                 treasury.rate_1_month,
                                 treasury.rate_3_month,
                                 treasury.rate_6_month,
                                 treasury.rate_1_year,
                                 treasury.rate_2_year,
                                 treasury.rate_3_year,
                                 treasury.rate_5_year,
                                 treasury.rate_7_year,
                                 treasury.rate_10_year,
                                 treasury.rate_20_year,
                                 treasury.rate_30_year))
            cursor.close()
            conn.commit()
            
        except MySQLdb.Error, e:
            print "create_treasury: Error %d: %s" % (e.args[0], e.args[1])
        return