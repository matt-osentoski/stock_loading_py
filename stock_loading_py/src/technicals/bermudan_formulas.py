'''
Created on Aug 18, 2010

@author: Matt.Osentoski

This module contains formulas based on bermudan equations

Converted to Python from "Financial Numerical Recipes in C" by:
Bernt Arne Odegaard
http://finance.bi.no/~bernt/gcc_prog/index.html
'''
import array
from math import sqrt
from math import exp
from math import pow

def option_price_call_bermudan_binomial(S, K, r, q, sigma, time, 
                                        potential_exercise_times, steps): 
    """Bermudan Option (Call) using binomial approximations
    @param S: spot (underlying) price
    @param K: strike (exercise) price
    @param r: interest rate
    @param q: artificial "probability"
    @param sigma: volatility 
    @param time: time to maturity 
    @param potential_exercise_times: Array of potential exercise times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)
    @param steps: Number of steps in binomial tree
    @return: Option price
    """                                            
    delta_t = time/steps
    R = exp(r*delta_t)           
    Rinv = 1.0/R                
    u = exp(sigma*sqrt(delta_t)) 
    uu = u*u
    d = 1.0/u
    p_up = (exp((r-q)*(delta_t))-d)/(u-d)
    p_down = 1.0-p_up
    prices = array.array('d', (0 for i in range(0,steps+1)))
    call_values = array.array('d', (0 for i in range(0,steps+1)))  
    
    potential_exercise_steps = [] # create list of steps at which exercise may happen
    for i in xrange(0, len(potential_exercise_times)):
        t = potential_exercise_times[i]
        if ( (t>0.0) and (t<time) ):
            potential_exercise_steps.append(int(t/delta_t))

    prices[0] = S*pow(d, steps) # fill in the endnodes.
    for i in xrange(1, steps+1):
        prices[i] = uu*prices[i-1]
    for i in xrange(0, steps+1):
        call_values[i] = max(0.0, (prices[i]-K))
    for step in xrange(steps-1, -1, -1):
        check_exercise_this_step = False
        for j in xrange(0, len(potential_exercise_steps)):
            if (step == potential_exercise_steps[j]):
                check_exercise_this_step = True
        for i in xrange(0, step+1):        
            call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv
            prices[i] = d*prices[i+1]
            if (check_exercise_this_step):
                call_values[i] = max(call_values[i],prices[i]-K)
                
    return call_values[0]   


def option_price_put_bermudan_binomial(S, X, r, q, sigma, time, 
                                        potential_exercise_times, steps): 
    """Bermudan Option (Call) using binomial approximations
    @param S: spot (underlying) price
    @param X: strike (exercise) price
    @param r: interest rate
    @param q: artificial "probability"
    @param sigma: volatility 
    @param time: time to maturity 
    @param potential_exercise_times: Array of potential exercise times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)
    @param steps: Number of steps in binomial tree
    @return: Option price
    """      
    delta_t=time/steps
    R = exp(r*delta_t)       
    Rinv = 1.0/R                
    u = exp(sigma*sqrt(delta_t))
    uu = u*u
    d = 1.0/u
    p_up = (exp((r-q)*delta_t)-d)/(u-d)
    p_down = 1.0-p_up 
    prices = array.array('d', (0 for i in range(0,steps+1)))
    put_values = array.array('d', (0 for i in range(0,steps+1)))  

    potential_exercise_steps = [] # create list of steps at which exercise may happen
    for i in xrange(0, len(potential_exercise_times)):
        t = potential_exercise_times[i]
        if ( (t>0.0) and (t<time) ):
            potential_exercise_steps.append(int(t/delta_t))

    prices[0] = S*pow(d, steps) # fill in the endnodes.
    for i in xrange(1, steps+1):
        prices[i] = uu*prices[i-1]
    for i in xrange(0, steps+1):
        put_values[i] = max(0.0, (X-prices[i])) # put payoffs at maturity
    for step in xrange(steps-1, -1, -1):
        check_exercise_this_step = False
        for j in xrange(0, len(potential_exercise_steps)):
            if (step == potential_exercise_steps[j]):
                check_exercise_this_step = True
        for i in xrange(0, step+1):
            put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv
            prices[i] = d*prices[i+1] 
            if (check_exercise_this_step):
                put_values[i] = max(put_values[i],X-prices[i])
            
    return put_values[0]