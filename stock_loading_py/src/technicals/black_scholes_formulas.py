'''
Created on Jul 19, 2010

@author: Matt.Osentoski

This module contains formulas based on the Black Scholes model

Converted to Python from "Financial Numerical Recipes in C" by:
Bernt Arne Odegaard
http://finance.bi.no/~bernt/gcc_prog/index.html
'''
from math import log
from math import sqrt
from math import pi
from math import exp

def n(z):  
    """Normal distribution
    @param z: Value to test
    @return: Normal distribution
    """
    return (1.0/sqrt(2.0*pi))*exp(-0.5*z*z)



def N(z):
    """Cumulative normal distribution
    Abramowiz and Stegun approximation (1964)
    @param z: Value to test
    @return: Cumulative normal distribution
    """
    if (z >  6.0):  # this guards against overflow 
        return 1.0 
    if (z < -6.0):
        return 0.0

    b1 =  0.31938153 
    b2 = -0.356563782
    b3 =  1.781477937
    b4 = -1.821255978
    b5 =  1.330274429
    p  =  0.2316419
    c2 =  0.3989423

    a=abs(z) 
    t = 1.0/(1.0+a*p) 
    b = c2*exp((-z)*(z/2.0))
    n = ((((b5*t+b4)*t+b3)*t+b2)*t+b1)*t
    n = 1.0-b*n 
    if ( z < 0.0 ): 
        n = 1.0 - n 
    return n


def option_price_call_black_scholes(S, K, r, sigma, time):
    """Black Scholes formula (Call)
    Black and Scholes (1973) and Merton (1973)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time: time to maturity 
    @return: Option price
    """                                                                          
    time_sqrt = sqrt(time)
    d1 = (log(S/K)+r*time)/(sigma*time_sqrt)+0.5*sigma*time_sqrt
    d2 = d1-(sigma*time_sqrt)
    return S*N(d1) - K*exp(-r*time)*N(d2)


def option_price_put_black_scholes(S, K, r, sigma, time):
    """Black Scholes formula (Put)
    Black and Scholes (1973) and Merton (1973)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time: time to maturity 
    @return: Option price
    """    
    time_sqrt = sqrt(time)
    d1 = (log(S/K)+r*time)/(sigma*time_sqrt) + 0.5*sigma*time_sqrt
    d2 = d1-(sigma*time_sqrt)
    return K*exp(-r*time)*N(-d2) - S*N(-d1)


def option_price_delta_call_black_scholes(S, K, r, sigma, time):
    """Delta of the Black Scholes formula (Call)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time: time to maturity 
    @return: Delta of the option
    """
    time_sqrt = sqrt(time)
    d1 = (log(S/K)+r*time)/(sigma*time_sqrt) + 0.5*sigma*time_sqrt
    delta = N(d1)
    return delta


def option_price_delta_put_black_scholes(S, K, r, sigma, time):
    """Delta of the Black Scholes formula (Put)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time: time to maturity 
    @return: Delta of the option
    """
    time_sqrt = sqrt(time)
    d1 = (log(S/K)+r*time)/(sigma*time_sqrt) + 0.5*sigma*time_sqrt
    delta = -N(-d1)
    return delta
  

def option_price_implied_volatility_call_black_scholes_bisections(S, K, r, time,
                                                                  option_price):
    """Calculates implied volatility for the Black Scholes formula using
    binomial search algorithm
    (NOTE: In the original code a large negative number was used as an
    exception handling mechanism.  This has been replace with a generic
    'Exception' that is thrown.  The original code is in place and commented
    if you want to use the pure version of this code)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param time: time to maturity 
    @param option_price: The price of the option
    @return: Sigma (implied volatility)
    """                                                             
    if (option_price<0.99*(S-K*exp(-time*r))):  # check for arbitrage violations. 
        return 0.0                           # Option price is too low if this happens
  
    # simple binomial search for the implied volatility.
    # relies on the value of the option increasing in volatility
    ACCURACY = 1.0e-5 # make this smaller for higher accuracy
    MAX_ITERATIONS = 100
    HIGH_VALUE = 1e10
    #ERROR = -1e40  // <--- original code
  
    # want to bracket sigma. first find a maximum sigma by finding a sigma
    # with a estimated price higher than the actual price.
    sigma_low=1e-5
    sigma_high=0.3
    price = option_price_call_black_scholes(S,K,r,sigma_high,time)
    while (price < option_price):  
        sigma_high = 2.0 * sigma_high # keep doubling.
        price = option_price_call_black_scholes(S,K,r,sigma_high,time)
        if (sigma_high>HIGH_VALUE):
            #return ERROR # panic, something wrong.  // <--- original code
            raise Exception("panic, something wrong.") # Comment this line if you uncomment the line above

    for i in xrange(0, MAX_ITERATIONS):
        sigma = (sigma_low+sigma_high)*0.5
        price = option_price_call_black_scholes(S,K,r,sigma,time)
        test = (price-option_price)
        if (abs(test)<ACCURACY):
            return sigma
        if (test < 0.0):
            sigma_low = sigma
        else:
            sigma_high = sigma
    #return ERROR      // <--- original code
    raise Exception("An error occurred") # Comment this line if you uncomment the line above
          
          
def option_price_implied_volatility_call_black_scholes_newton(S, K, r, time,
                                                              option_price):
    """Calculates implied volatility for the Black Scholes formula using
    the Newton-Raphson formula
    (NOTE: In the original code a large negative number was used as an
    exception handling mechanism.  This has been replace with a generic
    'Exception' that is thrown.  The original code is in place and commented
    if you want to use the pure version of this code)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param time: time to maturity 
    @param option_price: The price of the option
    @return: Sigma (implied volatility)
    """                                 
    if (option_price<0.99*(S-K*exp(-time*r))): # check for arbitrage violations. Option price is too low if this happens
        return 0.0

    MAX_ITERATIONS = 100
    ACCURACY = 1.0e-5
    t_sqrt = sqrt(time)

    sigma = (option_price/S)/(0.398*t_sqrt) # find initial value
    for i in xrange(0, MAX_ITERATIONS):
        price = option_price_call_black_scholes(S,K,r,sigma,time)
        diff = option_price -price
        if (abs(diff)<ACCURACY): 
            return sigma
        d1 = (log(S/K)+r*time)/(sigma*t_sqrt) + 0.5*sigma*t_sqrt
        vega = S * t_sqrt * n(d1)
        sigma = sigma + diff/vega
    #return -99e10 # something screwy happened, should throw exception // <--- original code
    raise Exception("An error occurred") # Comment this line if you uncomment the line above          


def option_price_partials_call_black_scholes(S, K, r, sigma, time):
    """Calculate partial derivatives for a Black Scholes Option (Call)
    (NOTE: Originally, this method used argument pointer references as a
    way of returning the partial derivatives in C++. I've removed these 
    references from the method signature and chose to return a tuple instead.)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time: time to maturity
    @return: Tuple of partial derivatives: (Delta, Gamma, Theta, Vega, Rho)
        Delta: partial wrt S
        Gamma: second partial wrt S
        Theta: partial wrt time
        Vega: partial wrt sigma
        Rho: partial wrt r
    """
    time_sqrt = sqrt(time)
    d1 = (log(S/K)+r*time)/(sigma*time_sqrt) + 0.5*sigma*time_sqrt
    d2 = d1-(sigma*time_sqrt)
    Delta = N(d1)
    Gamma = n(d1)/(S*sigma*time_sqrt)
    Theta =- (S*sigma*n(d1))/(2*time_sqrt) - r*K*exp( -r*time)*N(d2)
    Vega = S * time_sqrt*n(d1)
    Rho = K*time*exp(-r*time)*N(d2)
    return Delta, Gamma, Theta, Vega, Rho
 
 
def option_price_partials_put_black_scholes(S, K, r, sigma, time):
    """Calculate partial derivatives for a Black Scholes Option (Put)
    (NOTE: Originally, this method used argument pointer references as a
    way of returning the partial derivatives in C++. I've removed these 
    references from the method signature and chose to return a tuple instead.)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time: time to maturity
    @return: Tuple of partial derivatives: (Delta, Gamma, Theta, Vega, Rho)
        Delta: partial wrt S
        Gamma: second partial wrt S
        Theta: partial wrt time
        Vega: partial wrt sigma
        Rho: partial wrt r
    """
    time_sqrt = sqrt(time)
    d1 = (log(S/K)+r*time)/(sigma*time_sqrt) + 0.5*sigma*time_sqrt 
    d2 = d1-(sigma*time_sqrt)
    Delta = -N(-d1)
    Gamma = n(d1)/(S*sigma*time_sqrt)
    Theta = -(S*sigma*n(d1)) / (2*time_sqrt)+ r*K * exp(-r*time) * N(-d2) 
    Vega  = S * time_sqrt * n(d1)
    Rho   = -K*time*exp(-r*time) * N(-d2)
    return Delta, Gamma, Theta, Vega, Rho
 
 
def option_price_european_call_payout(S, X, r, q, sigma, time):
    """European option (Call) with a continuous payout. 
    The continuous payout would be for fees associated with the asset.
    For example, storage costs.
    @param S: spot (underlying) price
    @param X: strike (exercise) price,
    @param r: interest rate
    @param q: yield on underlying
    @param sigma: volatility 
    @param time: time to maturity
    @return: Option price
    """
    sigma_sqr = pow(sigma,2)
    time_sqrt = sqrt(time)
    d1 = (log(S/X) + (r-q + 0.5*sigma_sqr)*time)/(sigma*time_sqrt)
    d2 = d1-(sigma*time_sqrt)
    call_price = S * exp(-q*time)* N(d1) - X * exp(-r*time) * N(d2)
    return call_price                      


def option_price_european_put_payout(S, K, r, q, sigma, time): 
    """European option (Put) with a continuous payout. 
    The continuous payout would be for fees associated with the asset.
    For example, storage costs.
    @param S: spot (underlying) price
    @param X: strike (exercise) price,
    @param r: interest rate
    @param q: yield on underlying
    @param sigma: volatility 
    @param time: time to maturity
    @return: Option price
    """      
    sigma_sqr = pow(sigma,2)
    time_sqrt = sqrt(time)
    d1 = (log(S/K) + (r-q + 0.5*sigma_sqr)*time)/(sigma*time_sqrt)
    d2 = d1-(sigma*time_sqrt)
    put_price = K * exp(-r*time)*N(-d2)-S*exp(-q*time)*N(-d1)
    return put_price


def option_price_european_call_dividends(S, K, r, sigma, time_to_maturity,
                                         dividend_times, dividend_amounts ): 
    """European option for known dividends (Call)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time_to_maturity: time to maturity 
    @param dividend_times: Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)
    @param dividend_amounts: Array of dividend amounts for the 'dividend_times'
    @return: Option price
    """
    adjusted_S = S
    for i, div_times in enumerate(dividend_times):
        if (dividend_times[i]<=time_to_maturity):
            adjusted_S -= dividend_amounts[i] * exp(-r*dividend_times[i])
    return option_price_call_black_scholes(adjusted_S,K,r,sigma,time_to_maturity)


def option_price_european_put_dividends(S, K, r, sigma, time_to_maturity,
                                        dividend_times, dividend_amounts ):
    """European option for known dividends (Put)
    @param S: spot (underlying) price
    @param K: strike (exercise) price,
    @param r: interest rate
    @param sigma: volatility 
    @param time_to_maturity: time to maturity 
    @param dividend_times: Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)
    @param dividend_amounts: Array of dividend amounts for the 'dividend_times'
    @return: Option price
    """
    # reduce the current stock price by the amount of dividends. 
    adjusted_S=S
    for i, div_times in enumerate(dividend_times):
        if (dividend_times[i]<=time_to_maturity):
            adjusted_S -= dividend_amounts[i] * exp(-r*dividend_times[i])
    return option_price_put_black_scholes(adjusted_S,K,r,sigma,time_to_maturity)