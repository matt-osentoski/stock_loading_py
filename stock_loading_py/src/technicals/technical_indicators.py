from math import log
from math import pow
from math import sqrt

from numpy import std

from model.pivot_point import PivotPoint

class TechnicalIndicators:
    
    def get_pivot_point(self, high, low, close, open = None):
        pivot_point = PivotPoint()
        range = high - low
        
        p = 0
        if (open != None): # If an 'open' price was included, take it into account.
            p = (high + low + close + open) / 4
        else:
            p = (high + low + close) / 3
    
        pivot_point.p = p
        pivot_point.r1 = (p * 2) - low
        pivot_point.r2 = p + (range) 
        pivot_point.r3 = p + (range * 2)
        pivot_point.s1 = (p * 2) - high
        pivot_point.s2 = p - (range)
        pivot_point.s3 = p - (range * 2)
        return pivot_point
    
    def get_sma(self, stock_prices_array, idx, period):
        """Return the Simple moving average for a particular period
        - stock_prices_array An array of StockPrice objects
        - idx The index of the 'stock_prices_array' where the simple moving average applies
        - period The period of this moving average, for example 20 day moving average.
        """
        ma_array = []
        if (idx >= (period - 1)):
            start_ma_array = idx - (period -1)
            end_ma_array = start_ma_array + period
            ma_array = stock_prices_array[start_ma_array:end_ma_array]
            ma = (sum(x.closing_price for x in ma_array) / period)
            return ma
        return 0
    
    def get_ema(self, stock_prices_array, idx, period, previous_ema):
        smoothing_constant = 2.0 / (period + 1)
        
        ma_array =[]
        if (idx == (period - 1)):
            start_ma_array = idx - (period - 1)
            end_ma_array = start_ma_array + period
            ma_array = stock_prices_array[start_ma_array:end_ma_array]
            ma = (sum(x.closing_price for x in ma_array) / period)
            return ma
        elif (idx > (period - 1) and previous_ema != None):
            current_close = stock_prices_array[idx].closing_price
            ema = ( (float(smoothing_constant) * (float(current_close) - float(previous_ema))) + float(previous_ema))
            return ema
        else:
            return 0
        
    def get_macd(self, ema_12, ema_26):
        return (float(ema_12) - float(ema_26))
    
    def get_macd_ema(self, stock_prices_array, idx, period, previous_macd_ema):
        smoothing_constant = 2.0 / (period + 1)
        ma_array =[]
        if (idx == (period - 1)):
            start_ma_array = idx - (period - 1)
            end_ma_array = start_ma_array + period
            ma_array = stock_prices_array[start_ma_array:end_ma_array]
            ma = (sum(x.macd_12_26 for x in ma_array) / period)
            return ma
        elif (idx > (period -1) and previous_macd_ema != None):
            current_macd = stock_prices_array[idx].macd_12_26
            ema = ((smoothing_constant * (current_macd - previous_macd_ema)) + 
                   previous_macd_ema)
            return ema
        else:
            return 0
            
    def get_macd_histogram(self, macd, ema_9):
        return (float(macd) - float(ema_9))
    
    def get_vol_sma(self, stock_prices_array, idx, period):
        """Return the Volume simple moving average for a particular period
        - stock_prices_array An array of StockPrice objects
        - idx The index of the 'stock_prices_array' where the simple moving average applies
        - period The period of this moving average, for example 20 day moving average.
        """
        ma_array = []
        if (idx >= (period - 1)):
            start_ma_array = idx - (period -1)
            end_ma_array = start_ma_array + period
            ma_array = stock_prices_array[start_ma_array:end_ma_array]
            ma = (sum(x.volume for x in ma_array) / period)
            return ma
        return 0
    
    def get_ppo(self, ema_12, ema_26):
        if (ema_26 == 0):
            return 0
        return (float(ema_12) - float(ema_26)) / float(ema_26)
    
    def get_ppo_ema(self, stock_prices_array, idx, period, previous_ppo_ema):
        smoothing_constant = 2.0 / (period + 1)
        ma_array =[]
        if (idx == (period - 1)):
            start_ma_array = idx - (period - 1)
            end_ma_array = start_ma_array + period
            ma_array = stock_prices_array[start_ma_array:end_ma_array]
            ma = (sum(x.ppo_12_26 for x in ma_array) / period)
            return ma
        elif (idx > (period -1) and previous_ppo_ema != None):
            current_ppo = stock_prices_array[idx].ppo_12_26
            ema = ((smoothing_constant * (current_ppo - previous_ppo_ema)) + 
                   previous_ppo_ema)
            return ema
        else:
            return 0
        
    def get_ppo_histogram(self, ppo, ema_9):
        return (ppo - ema_9)
    
    def get_rsi(self, stock_prices_array, idx, period = 14):
        """Returns the Relative Strength Index (RSI)
        - stock_prices_array An array of StockPrice objects
        - idx The index of the 'stock_prices_array' of this RSI value
        - period The period of this RSI calculation, for example 14."""
    
        # The RSI of the first calculation is different than subsequent calculations
        # The first calculation of RSI begins when you have a number of closing prices equal to the
        # period you're looking at.  In the default case, after 14 closing prices (Actually this will
        # be after 15 closing prices, because you have to compare the first closing price with something
        # to determine if there was a gain or loss.
        if idx >= (period):    
            gain_loss_array = stock_prices_array[1:period+1]
            gains = []
            losses = []
      
            previous_price = stock_prices_array[0].closing_price
            previous_avg_gain = 0
            previous_avg_loss = 0
      
            for x, stock_price in enumerate(gain_loss_array):     
                price_change = stock_price.closing_price - previous_price
                if (price_change < 0):
                    losses.append(abs(price_change))  # losses are represented as positive numbers
                else:
                    gains.append(price_change)

                previous_price = stock_price.closing_price # Set this price as the new previous_price for the next iteration
            
            # end for... gain_loss_array    
            avg_gain = sum(gains) / period 
            avg_loss = sum(losses) / period 
            previous_avg_gain = avg_gain
            previous_avg_loss = avg_loss
  
            first_rs = avg_gain / avg_loss
            first_rsi = 0
            if (avg_loss == 0):
                first_rsi = 100
            else:
                first_rsi = (100 - (100 / (1 + first_rs) ))

            if idx == (period):
                return float(first_rsi)

            # Now that the first RSI value has been established, the running RSI calculation can begin
            for x, stock_price in enumerate(stock_prices_array):
                if (x <= period):
                    continue  # We dealt with the first series of this array in the code above.

                price_change = stock_price.closing_price - previous_price
        
                # Now that the price change has been calculated, set the current price to the previous price
                # for the next iteration.
                previous_price = stock_price.closing_price
        
                gain = 0
                loss = 0
                if (price_change < 0):
                    loss = abs(price_change)  # losses are represented as positive numbers
                else:
                    gain = price_change
     
                avg_gain = ((previous_avg_gain * (period-1)) + gain) / period
                avg_loss = ((previous_avg_loss * (period-1)) + loss) / period
        
                # Now that the average gain/loss has been established, set the current gain/loss to the 
                # previous_avg_gain/loss for the next iteration.
                previous_avg_gain = avg_gain
                previous_avg_loss = avg_loss
        
                rs = avg_gain / avg_loss
                rsi = 0
                if (avg_loss == 0):
                    rsi = 100
                else:
                    rsi = (100 - (100 / (1 + rs) ))
        
                if (idx == x):
                    return float(rsi)

            # stock_prices_array.each_...
            return 0  # If for some reason a value hasn't been returned, set RSI to 0
      
        else: # idx >= (period...
            return 0

    def get_stochastics(self, stock_prices_array, idx, k_period = 10, d_period = 3):
        """Returns the Stochastics
        - stock_prices_array An array of StockPrice objects
        - idx The index of the stock_prices_array of this RSI value
        - k_period The period for %K (example 10)
        - d_period The period for %D (example 3)"""
        
        if idx >= (k_period - 1):  
            # The array below will keep track of all numerators and denominators used
            # to calculate %K, these values will then be used to calculate %D
            fast_k_arr = []
            # This array keeps track of slow %K to later calculate slow %D
            slow_k_arr = []
     
            for x, stock_price in enumerate(stock_prices_array):   
                if (x >= (k_period - 1)):
                    start_period_arr = x - (k_period - 1)
                    end_period_arr = start_period_arr + k_period
                    period_arr = stock_prices_array[start_period_arr:end_period_arr]
                    max_high = max(o.high_price for o in period_arr)
                    min_low = min(o.low_price for o in period_arr)
                    num = stock_price.closing_price - min_low
                    den = max_high - min_low
                    k = 0
                    if (num == 0):
                        k = 0
                    else:
                        k = 100 * (num / den)
                    fast_k_arr.append(k)
                    d = 0
          
                    # If there is enough old data, calculate %D
                    if (len(fast_k_arr) >= d_period):
                        fast_k_tmp_start = len(fast_k_arr) - d_period
                        fast_k_tmp_stop = fast_k_tmp_start + d_period
                        fast_k_arr_trimmed = fast_k_arr[
                            fast_k_tmp_start:fast_k_tmp_stop]
                        d = sum(fast_k_arr_trimmed) / d_period
                    else:
                        d = 0     
          
                    # Calculate Stochastics Slow
                    # %D (Fast) is the %K slow
                    k_slow = d
                    if (k_slow != 0):
                        slow_k_arr.append(k_slow)
                    d_slow = 0
          
                    # If there is enough old data, calculate %D (slow)
                    if (len(slow_k_arr) == d_period):  
                        d_slow = sum(slow_k_arr) / d_period 
                    elif (len(slow_k_arr) > d_period):
                        tmp_slow_k_arr_start = len(slow_k_arr) - d_period
                        tmp_slow_k_arr_stop = tmp_slow_k_arr_start + d_period
                        tmp_slow_k_arr = slow_k_arr[
                            tmp_slow_k_arr_start:tmp_slow_k_arr_stop]
                        d_slow = sum(tmp_slow_k_arr) / d_period              
                    else:
                        d_slow = 0 
 
                    # If this is the index we want, return all the values
                    if (idx == x):           
                        return float(k), float(d), float(k_slow), float(d_slow)
          
                #end if (x >= (k_period...
            #end for x, stock_price...   
        else:
            return 0,0,0,0

    def get_atr(self, stock_prices_array, idx, previous_atr = 0, period = 14):
        if (idx >= period - 1):
            prev_close = 0
            daily_tr_arr = []
            for x, stock_price in enumerate(stock_prices_array):
                daily_tr = 0
                # For the first entry, the Daily TR is the 'High' - 'Low'
                if (x == 0):
                    daily_tr = stock_price.high_price - stock_price.low_price
                else:
                    high_low = stock_price.high_price - stock_price.low_price
                    high_prev_close = abs(stock_price.high_price - prev_close)
                    low_prev_close = abs(stock_price.low_price - prev_close)
                    max_tmp_arr = [high_low, high_prev_close, low_prev_close]
                    daily_tr = max(max_tmp_arr)       
            
                # Make the prev close, the current prev close and add the TR
                # to the TR array.
                prev_close = stock_price.closing_price       
                daily_tr_arr.append(daily_tr)
                
                # Slice off the daily_tr_arr to the period and generate an average
                # if the index is the period value, otherwise, use the smoothed out average. 
                if (idx == period - 1 and x == idx):      
                    tmp_daily_tr_arr_st = len(daily_tr_arr) - period
                    tmp_daily_tr_arr_end = tmp_daily_tr_arr_st + period
                    tmp_daily_tr_arr = daily_tr_arr[
                        tmp_daily_tr_arr_st:tmp_daily_tr_arr_end]
                    atr = sum(tmp_daily_tr_arr) / period 
                    return atr
                elif (idx > period - 1 and x == idx):
                    atr = (previous_atr * (period -1) + daily_tr) / period
                    return atr 
        else:
            return 0
        
    def get_historical_volatility(self, stock_prices_array, idx, period=20, 
                                  trading_days=252):
        """Simple moving average volatility.
        @param stock_prices_array: An array of StockPrice objects
        @param idx: The index of the 'stock_prices_array' where this calculation applies
        @param period: The # of days to go back to calculate volatility
        @param trading_days: The # of trading days to use in the calculation (252 is for annualized volatility)
        @return: Volatility in decimal form.  Multiply by 100 to get a percentage.
        """
        if (idx >= period):  # User 'period', instead of (period -1) since the first calc is period+1
            # Build the price change array
            price_changes = []
            last_price = 0
            for x, stock_price in enumerate(stock_prices_array):
                if (x == 0):
                    price_changes.append(0);
                    last_price = stock_price.closing_price
                else:
                    price_change = log(stock_price.closing_price/last_price)
                    price_changes.append(price_change)
                    last_price = stock_price.closing_price
            # Grab an array of price changes over the selected period
            tmp_price_changes_st = idx - (period - 1)
            tmp_price_changes_end = tmp_price_changes_st + period
            tmp_price_changes = price_changes[tmp_price_changes_st:tmp_price_changes_end]
            # Setting 'ddof=1' adds 1 degree of freedom, making this std 
            # equavalent to Excel's STDEV
            std_dev = std(tmp_price_changes, ddof=1) 
            return std_dev * sqrt(trading_days)        
        else:
            return 0
        
    def get_ewma_volatility(self, stock_prices_array, idx, trading_days=252, 
                            lambda_const=0.94, array_size=252):
        """Exponential weighted moving average volatility
        This calculation was derived from a spreadsheet at Bionic Turtle
        (http://www.bionicturtle.com/)
        @param stock_prices_array: An array of StockPrice objects
        @param idx: The index of the 'stock_prices_array' where this calculation applies
        @param trading_days: The # of days used in the calculation (252 for annualized volatility)
        @param lambda_const: The lambda value used for the calculation. Defaults to the Risk Metrics value of 94%
        @param array_size: The number of days to go back in the history of prices to make this calculation.
        @return: Volatility of the exponentially weighted moving average in decimal form. Multiply by 100 to get a percentage.
        """
        adj_array_size = array_size
        if (len(stock_prices_array) < array_size):
            adj_array_size = len(stock_prices_array)
        
        period_returns = []
        period_returns_sq = [] # Returns squared, which is the same as simple volatility
        
        # Create a subset of the prices based on the array_size
        tmp_price_st = idx - (adj_array_size -1)
        tmp_price_end = tmp_price_st + adj_array_size
        tmp_stock_prices = stock_prices_array[tmp_price_st:tmp_price_end]
        
        last_price = 0
        for x, stock_price in enumerate(tmp_stock_prices):
            if (x == 0):
                period_returns.append(0.0)
                period_returns_sq.append(0.0)
                last_price = stock_price.closing_price
            else:
                price_change = log(stock_price.closing_price/last_price)
                period_returns.append(price_change)
                period_returns_sq.append(pow(price_change,2))
                last_price = stock_price.closing_price
        

        weight_vars = [] # Weighted variances for day 'N'
        weight_vars_prev = [] # Weighted variances for day 'N-1'
        
        weight = 0 # The weight for day 'N'
        weight_prev = 0 # The weight for day 'N-1' 
        
        # Use a reverse loop to run through the prices from most recent to oldest
        for i, stock in reversed(list(enumerate(tmp_stock_prices))):
            if (i == (len(tmp_stock_prices) - 2) ): # Weight for day 'N'
                weight = 1.0 - lambda_const
                weight_vars.append(period_returns_sq[i] * weight)
            elif (i == (len(tmp_stock_prices) - 3)): #Weight for day 'N-1'
                weight_prev = 1.0 - lambda_const
                weight = weight * lambda_const
                weight_vars.append(period_returns_sq[i] * weight)
                weight_vars_prev.append(period_returns_sq[i] * weight_prev)
            elif (i < (len(tmp_stock_prices) - 3)):    
                weight = weight * lambda_const
                weight_prev = weight_prev * lambda_const
                weight_vars.append(period_returns_sq[i] * weight)
                weight_vars_prev.append(period_returns_sq[i] * weight_prev)
        
        sum_vars = sum(weight_vars)
        sum_vars_prev = sum(weight_vars_prev)
        ewma = lambda_const * sum_vars_prev + (1 - lambda_const) * period_returns_sq[adj_array_size - 2]
        return sqrt(ewma) * sqrt(trading_days)
   
    def get_garch_1_1_volatility(self, stock_prices_array, idx, long_run_var, 
                            alpha, beta, lambda_const = 0.94, trading_days=252, 
                            array_size=252):
        """Calculates volatility using the GARCH(1,1) formula.
        This calculation was derived from a spreadsheet at Bionic Turtle
        (http://www.bionicturtle.com/)
        (NOTE: Gamma is determined using:  1 - alpha - beta)
        @param stock_prices_array: An array of StockPrice objects
        @param idx: The index of the 'stock_prices_array' where this calculation applies
        @param long_run_var: The long run variance for this series of prices
        @param alpha: The Alpha weight used for this calculation
        @param beta: The Beta weight used for this calculation
        @param lambda_const: The lambda value used for the calculation. Defaults to the Risk Metrics value of 94%
        @param trading_days: The # of days used in the calculation (252 for annualized volatility)
        @param array_size: The number of days to go back in the history of prices to make this calculation.
        @return: Volatility of the GARCH(1,1) formula in decimal form. Multiply by 100 to get a percentage.
        """
        adj_array_size = array_size
        if (len(stock_prices_array) < array_size):
            adj_array_size = len(stock_prices_array)
        
        # The three weights Alpha + Beta + Gamma can not be greater than 1
        # Throw an exception if this occurs.
        if (alpha + beta > 1.0):
            raise Exception("The weights Alpha + Beta + Gamma should not be greater than 1")
        gamma = 1 - alpha - beta
        
        period_returns = []
        period_returns_sq = [] # Returns squared, which is the same as simple volatility
        
        # Create a subset of the prices based on the array_size
        tmp_price_st = idx - (adj_array_size -1)
        tmp_price_end = tmp_price_st + adj_array_size
        tmp_stock_prices = stock_prices_array[tmp_price_st:tmp_price_end]
        
        last_price = 0
        for x, stock_price in enumerate(tmp_stock_prices):
            if (x == 0):
                period_returns.append(0.0)
                period_returns_sq.append(0.0)
                last_price = stock_price.closing_price
            else:
                price_change = log(stock_price.closing_price/last_price)
                period_returns.append(price_change)
                period_returns_sq.append(pow(price_change,2))
                last_price = stock_price.closing_price
        

        weight_vars = [] # Weighted variances for day 'N'
        weight_vars_prev = [] # Weighted variances for day 'N-1'
        
        weight = 0 # The weight for day 'N'
        weight_prev = 0 # The weight for day 'N-1' 
        
        # Use a reverse loop to run through the prices from most recent to oldest
        for i, stock in reversed(list(enumerate(tmp_stock_prices))):
            if (i == (len(tmp_stock_prices) - 2) ): # Weight for day 'N'
                weight = 1.0 - lambda_const
                weight_vars.append(period_returns_sq[i] * weight)
            elif (i == (len(tmp_stock_prices) - 3)): #Weight for day 'N-1'
                weight_prev = 1.0 - lambda_const
                weight = weight * lambda_const
                weight_vars.append(period_returns_sq[i] * weight)
                weight_vars_prev.append(period_returns_sq[i] * weight_prev)
            elif (i < (len(tmp_stock_prices) - 3)):    
                weight = weight * lambda_const
                weight_prev = weight_prev * lambda_const
                weight_vars.append(period_returns_sq[i] * weight)
                weight_vars_prev.append(period_returns_sq[i] * weight_prev)
        
        sum_vars = sum(weight_vars)
        sum_vars_prev = sum(weight_vars_prev)
        garch_1_1 = (long_run_var * gamma) + (alpha * 
                period_returns_sq[adj_array_size - 2]) + (beta * sum_vars_prev)
        return sqrt(garch_1_1) * sqrt(trading_days)