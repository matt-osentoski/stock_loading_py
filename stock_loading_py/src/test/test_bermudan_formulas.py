'''
Created on Aug 19, 2010

@author: Matt.Osentoski
'''
import unittest

from technicals.bermudan_formulas import *

class TestBermudanFormulas(unittest.TestCase):

    def test_option_price_call_bermudan_binomial(self):
        S = 80
        K = 100
        r = 0.20
        q = 0.0
        sigma = 0.25
        time = 1.0
        steps = 500
        potential_exercise_times = [0.25, 0.5, 0.75]
        test_val = option_price_call_bermudan_binomial(S, K, r, q, sigma, time, 
                                        potential_exercise_times, steps)
        self.assertEqual(str(round(test_val, 5)), "7.14016")

    def test_option_price_put_bermudan_binomial(self):
        S = 80
        K = 100
        r = 0.20
        q = 0.0
        sigma = 0.25
        time = 1.0
        steps = 500
        potential_exercise_times = [0.25, 0.5, 0.75]
        test_val = option_price_put_bermudan_binomial(S, K, r, q, sigma, time, 
                                        potential_exercise_times, steps)
        self.assertEqual(str(round(test_val, 4)), "15.8869")
