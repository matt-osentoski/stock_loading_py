'''
Created on Jul 21, 2010

@author: Matt.Osentoski
'''
import unittest

from technicals.binomial_formulas import *

class TestBinomialFormulas(unittest.TestCase):
    
    def test_option_price_call_american_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        test_val = option_price_call_american_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(test_val, 4)), "14.9505")
        # Test 2
        S = 72
        K = 72
        r = 0.05
        sigma = 0.40
        time = 0.5
        steps = 200
        test_val = option_price_call_american_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(test_val, 5)), "8.90719")
        
        
    def test_option_price_put_american_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        test_val = option_price_put_american_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(test_val, 5)), "6.54691")
        # Test 2
        S = 72
        K = 72
        r = 0.05
        sigma = 0.40
        time = 0.5
        steps = 200
        test_val = option_price_put_american_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(test_val, 5)), "7.29582")
        
        
    def test_option_price_delta_american_call_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        test_val = option_price_delta_american_call_binomial(S, K, r, sigma, 
                                                             time, steps)
        self.assertEqual(str(round(test_val, 6)), "0.699792")
        
        
    def test_option_price_delta_american_put_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        test_val = option_price_delta_american_put_binomial(S, K, r, sigma, 
                                                                time, steps)
        self.assertEqual(str(round(test_val, 6)), "-0.387636")
        
        
    def test_option_price_partials_american_call_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        delta, gamma, theta, vega, rho = option_price_partials_american_call_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(delta, 6)), "0.699792")
        self.assertEqual(str(round(gamma, 7)), "0.0140407")
        self.assertEqual(str(round(theta, 5)), "-9.89067")
        self.assertEqual(str(round(vega, 4)), "34.8536")
        self.assertEqual(str(round(rho, 4)), "56.9652")
        
        
    def test_option_price_partials_american_put_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        delta, gamma, theta, vega, rho = option_price_partials_american_put_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(delta, 6)), "-0.387636")
        self.assertEqual(str(round(gamma, 7)), "0.0209086")
        self.assertEqual(str(round(theta, 5)), "-1.99027")
        self.assertEqual(str(round(vega, 4)), "35.3943")
        self.assertEqual(str(round(rho, 4)), "-21.5433")
        
        
    def test_option_price_call_american_discrete_dividends_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        dividend_times = [0.25, 0.75]
        dividend_amounts = [2.5, 2.5]
        test_val = option_price_call_american_discrete_dividends_binomial(S, K, 
                    r, sigma, time, steps, dividend_times, dividend_amounts)
        self.assertEqual(str(round(test_val, 4)), "12.0233")
        
    
    def test_option_price_put_american_discrete_dividends_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        dividend_times = [0.25, 0.75]
        dividend_amounts = [2.5, 2.5]
        test_val = option_price_put_american_discrete_dividends_binomial(S, K, 
                    r, sigma, time, steps, dividend_times, dividend_amounts)
        self.assertEqual(str(round(test_val, 5)), "8.11801")
        
    def test_option_price_call_american_proportional_dividends_binomial(self):    
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        dividend_times = [0.25, 0.75]
        dividend_yields = [0.025, 0.025]
        test_val = option_price_call_american_proportional_dividends_binomial(S, 
                K, r, sigma, time, steps, dividend_times, dividend_yields)
        self.assertEqual(str(round(test_val, 4)), "11.8604")
        
    def test_option_price_put_american_proportional_dividends_binomial(self):    
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        dividend_times = [0.25, 0.75]
        dividend_yields = [0.025, 0.025]
        test_val = option_price_put_american_proportional_dividends_binomial(S, 
                K, r, sigma, time, steps, dividend_times, dividend_yields)
        self.assertEqual(str(round(test_val, 5)), "7.99971")
        
    
    def test_option_price_call_american_binomial_payout(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        y = 0.02
        time = 1.0
        steps = 100
        test_val = option_price_call_american_binomial_payout(S, K, r, y, 
                                                        sigma, time, steps)
        self.assertEqual(str(round(test_val, 4)), "13.5926")
        
        
    def test_option_price_put_american_binomial_payout(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        y = 0.02
        time = 1.0
        steps = 100
        test_val = option_price_put_american_binomial_payout(S, K, r, y, 
                                                        sigma, time, steps)
        self.assertEqual(str(round(test_val, 5)), "6.99407")
        
    def test_option_price_call_european_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        test_val = option_price_call_european_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(test_val, 4)), "14.9505")
        
        
    def test_option_price_put_european_binomial(self):
        S = 100
        K = 100
        r = 0.10
        sigma = 0.25
        time = 1.0
        steps = 100
        test_val = option_price_put_european_binomial(S, K, r, sigma, time, steps)
        self.assertEqual(str(round(test_val, 5)), "5.43425")