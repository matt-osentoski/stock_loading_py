'''
Created on Jun 9, 2010

@author: Matt.Osentoski
'''
import unittest

from model.stock import Stock
from model.stock_price import StockPrice
from technicals.technical_indicators import TechnicalIndicators

class TestTechnicalInidicators(unittest.TestCase):
    '''
    classdocs
    '''
#    def __init__(selfparams):
#        '''
#        Constructor
#        '''
    def test_get_sma(self):
        ti = TechnicalIndicators()
        price_arr = [59.375, 58.9062, 54.3125, 51, 51.5938, 52, 55.4375, 
                     52.9375, 54.5312, 56.5312, 55.3438, 55.7188, 50.1562]
        prices = []
        for price in price_arr:
            sp = StockPrice()
            sp.closing_price = price
            prices.append(sp)
            
        test_price = ti.get_sma(prices, 6, 3)
        self.assertEqual(str(round(test_price, 5)), "53.01043")
        
    def test_get_ema(self):
        ti = TechnicalIndicators()
        price_arr = [64.75, 63.79, 63.73, 63.73, 63.55, 63.19, 63.91, 63.85, 
                     62.95, 63.37, 61.33, 61.51, 61.87, 60.25, 59.35, 59.95, 
                     58.93, 57.68, 58.82, 58.87]
        prices = []
        for price in price_arr:
            sp = StockPrice()
            sp.closing_price = price
            prices.append(sp)
        emas = []
        prev_ema = 0
        for x, ema in enumerate(prices):
            tmp_ema = ti.get_ema(prices, x, 10, prev_ema)
            prev_ema = tmp_ema
            emas.append(tmp_ema)
        self.assertEqual(str(round(emas[14], 3)), "61.755")
    
    def test_get_macd(self):
        ti = TechnicalIndicators()
        ema_12 = 29.24
        ema_26 = 28.21
        macd = ti.get_macd(ema_12, ema_26)
        self.assertEqual(str(round(macd, 2)), "1.03")
        
    def test_get_macd_ema(self):
        ti = TechnicalIndicators()
        macd_12_26_arr = [64.75, 63.79, 63.73, 63.73, 63.55, 63.19, 63.91, 63.85, 
                     62.95, 63.37, 61.33, 61.51, 61.87, 60.25, 59.35, 59.95, 
                     58.93, 57.68, 58.82, 58.87]
        macd_arr = []
        for macd in macd_12_26_arr:
            sp = StockPrice()
            sp.macd_12_26 = macd
            macd_arr.append(sp)
        signals = []
        prev_signal = 0
        for x, m in enumerate(macd_arr):
            tmp_signal = ti.get_macd_ema(macd_arr, x, 9, prev_signal)
            prev_signal = tmp_signal
            signals.append(tmp_signal)
        self.assertEqual(str(round(signals[14], 3)), "61.608")
        
    def test_get_macd_histogram(self):
        ti = TechnicalIndicators()
        macd = 1.01
        ema_9 = 0.88
        macd_hist = ti.get_macd_histogram(macd, ema_9)
        self.assertEqual(str(round(macd_hist, 2)), "0.13")
    
    def test_get_vol_sma(self):
        ti = TechnicalIndicators()
        vol_arr = [2981600, 2346400, 5444000, 4773600, 7429600, 8826400,
                   6886400, 3495200, 3816000, 2387200, 3591200, 2833600,
                   5977600, 3987200, 3796000, 8344800, 3521600]
        volumes = []
        for vol in vol_arr:
            sp = StockPrice()
            sp.volume = vol
            volumes.append(sp)
            
        test_price = ti.get_vol_sma(volumes, 6, 3)
        self.assertEqual(int(round(test_price)), 7714133)
        
    def test_get_ppo(self):
        ti = TechnicalIndicators()
        ema_12 = 227.69
        ema_26 = 221.28
        ppo = ti.get_ppo(ema_12, ema_26)
        self.assertEqual(str(round(ppo, 5)), "0.02897")
        
    def test_get_ppo_ema(self):
        ti = TechnicalIndicators()
        ppo_12_26_arr = [64.75, 63.79, 63.73, 63.73, 63.55, 63.19, 63.91, 63.85, 
                     62.95, 63.37, 61.33, 61.51, 61.87, 60.25, 59.35, 59.95, 
                     58.93, 57.68, 58.82, 58.87]
        ppo_arr = []
        for ppo in ppo_12_26_arr:
            sp = StockPrice()
            sp.ppo_12_26 = ppo
            ppo_arr.append(sp)
        signals = []
        prev_signal = 0
        for x, p in enumerate(ppo_arr):
            tmp_signal = ti.get_ppo_ema(ppo_arr, x, 9, prev_signal)
            prev_signal = tmp_signal
            signals.append(tmp_signal)
        self.assertEqual(str(round(signals[14], 3)), "61.608")
        
    def test_get_ppo_histogram(self):
        ti = TechnicalIndicators()
        ppo = 0.03158
        ema_9 = 0.02832
        ppo = ti.get_ppo_histogram(ppo, ema_9)
        self.assertEqual(str(round(ppo, 5)), "0.00326")
        
    def test_get_rsi(self):
        ti = TechnicalIndicators()
        price_arr = [46.1250, 47.1250, 46.4375, 46.9375, 44.9375, 44.2500, 
                     44.6250, 45.7500, 47.8125, 47.5625, 47.0000, 44.5625, 
                     46.3125, 47.6875, 46.6875, 45.6875, 43.0625, 43.5625, 
                     44.8750, 43.6875]
        stock_price_arr = []
        for price in price_arr:
            stock_price = StockPrice()
            stock_price.closing_price = price
            stock_price_arr.append(stock_price)
        rsi = ti.get_rsi(stock_price_arr, 19)
        self.assertEqual(str(round(rsi, 4)), "43.9921")
        
    def test_get_stochastics(self):
        ti = TechnicalIndicators()
        high_arr = [421.89, 421.4, 421.62, 418.85, 420.35, 414.85, 411.64, 
                      413.61, 415.83, 414.95, 415.29, 416.07, 418.28, 420.31, 
                      418.62, 417.18, 416.44, 420.52, 420.58, 425.27, 425.22, 
                      422.44, 421.43]
        low_arr = [419.44, 419.78, 418.19, 416.93, 413.58, 410.7, 408.3, 
                   410.53, 413.51, 413.38, 413.76, 413.35, 415.31, 417.49, 
                   416.76, 414.3, 414.44, 416.34, 419.13, 419.58, 419.54, 
                   417.77, 419.62]
        close_arr = [420.74, 421.34, 418.19, 418.26, 414.85, 410.72, 411.61, 
                     413.51, 413.53, 414.84, 414.03, 416.07, 417.98, 417.98, 
                     417.08, 414.44, 416.36, 419.95, 419.58, 425.27, 419.77, 
                     419.92, 419.93]
        stock_price_arr = []
        for x, price in enumerate(high_arr):
            stock_price = StockPrice()
            stock_price.high_price = price
            stock_price.low_price = low_arr[x]
            stock_price.closing_price = close_arr[x]
            stock_price_arr.append(stock_price)
        k, d, k_slow, d_slow = ti.get_stochastics(stock_price_arr, 22)
        self.assertEqual(str(round(k, 2)), "51.32") # Test %K
        self.assertEqual(str(round(d, 2)), "52.14") # Test %D
        self.assertEqual(str(round(k_slow, 2)), "52.14") # Test %K slow
        self.assertEqual(str(round(d_slow, 2)), "66.84") # Test %D slow
        
    def test_get_atr(self):
        ti = TechnicalIndicators()
        high_arr = [61, 61, 58.8438, 55.125, 54.0625, 53.9688, 56, 55.2188, 
                    55.0312, 57.4922, 57.0938, 56.8125, 55.5625, 50.0625, 
                    47.6875, 44.9062, 47.375, 47.625, 48.0156, 45.0391, 43.75, 
                    43.25, 43, 42.5, 44.875, 44.3125, 41.2188, 39.375, 40.9375, 
                    40.5938, 46, 48.125, 45]
        low_arr = [59.0312, 58.375, 53.625, 47.4375, 50.5, 49.7812, 52.5, 
                   52.625, 53.25, 53.75, 55.25, 54.3438, 50, 46.8438, 44.4688, 
                   40.625, 44.1641, 45.125, 43.25, 42.6875, 40.75, 39.9688, 40, 
                   40.75, 43.375, 40.0625, 37.625, 36.5, 37.5625, 36.9375, 
                   40.8438, 42.5625, 42.5]
        close_arr = [59.375, 58.9062, 54.3125, 51, 51.5938, 52, 55.4375, 
                     52.9375, 54.5312, 56.5312, 55.3438, 55.7188, 50.1562, 
                     48.8125, 44.5938, 42.6562, 47, 46.9688, 43.625, 44.6562, 
                     40.8125, 42.5625, 40, 42.4375, 44.0938, 40.625, 39.875, 
                     38.0312, 38.4688, 39.4375, 45.875, 44.25, 42.8125]
        stock_price_arr = []

        for x, price in enumerate(high_arr):
            stock_price = StockPrice()
            stock_price.high_price = price
            stock_price.low_price = low_arr[x]
            stock_price.closing_price = close_arr[x] 
            stock_price_arr.append(stock_price)
            
        prev_atr = 0
        atr_results = []
        for x, price in enumerate(stock_price_arr):
            atr = ti.get_atr(stock_price_arr, x, prev_atr, 14)
            prev_atr = atr
            atr_results.append(atr)
        self.assertEqual(str(round(atr_results[32], 5)), "3.77148")
        
        
    def test_get_historical_volatility(self):
        ti = TechnicalIndicators()
        close_arr = [31.65, 31.97,32.25,32.28,34.62,34.48,32.28,32.73,34.9,
                     35.1,35.33,34.94,35.23,35.24,35.38,36.03,36.12,36.32,
                     36.99,38.45,38.76,39.81,38.9,39.42,39.47,40.45,39.37,
                     39.18,40.6,42.31]
        stock_price_arr = []
        
        for x, price in enumerate(close_arr):
            stock_price = StockPrice()
            stock_price.closing_price = close_arr[x] 
            stock_price_arr.append(stock_price)
        vola = ti.get_historical_volatility(stock_price_arr, 28, 10, 252)
        self.assertEqual(str(round(vola, 6)), "0.364606")
        
    def test_get_ewma_volatility(self):
        ti = TechnicalIndicators()
        close_arr = [100.0, 105.0, 110.0, 100.0, 110.0, 120.0, 100.0, 125.0, 
                     90.0, 130.0, 85.0, 140.0]
        stock_price_arr = []
        
        for x, price in enumerate(close_arr):
            stock_price = StockPrice()
            stock_price.closing_price = close_arr[x] 
            stock_price_arr.append(stock_price)
        vola = ti.get_ewma_volatility(stock_price_arr, 11, 1, 0.90, 252)
        self.assertEqual(str(round(vola, 5)), "0.21523")
        
    def test_get_garch_1_1_volatility(self):
        ti = TechnicalIndicators()
        close_arr = [100.0, 105.0, 110.0, 100.0, 110.0, 120.0, 100.0, 125.0, 
                     90.0, 130.0, 85.0, 140.0]
        stock_price_arr = []
        
        for x, price in enumerate(close_arr):
            stock_price = StockPrice()
            stock_price.closing_price = close_arr[x] 
            stock_price_arr.append(stock_price)
        vola = ti.get_garch_1_1_volatility(stock_price_arr, 11, 0.05, 0.10, 
                                           0.80, 0.90, 1, 252)
        self.assertEqual(str(round(vola, 5)), "0.21951")