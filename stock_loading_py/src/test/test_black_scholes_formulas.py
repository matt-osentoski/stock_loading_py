'''
Created on Jul 21, 2010

@author: Matt.Osentoski
'''
import unittest

from technicals.black_scholes_formulas import *

class TestBlackScholesFormulas(unittest.TestCase):
    
    def test_n(self):
        test_val = n(1.23)
        self.assertEqual(str(round(test_val, 6)), "0.187235")
      
    
    def test_N(self):
        test_val = N(1.23)
        self.assertEqual(str(round(test_val, 6)), "0.890651")
      
        
    def test_option_price_call_black_scholes(self):
        S = 50
        K = 50
        r = 0.10
        sigma = 0.30
        time = 0.50
        test_val = option_price_call_black_scholes(S, K, r, sigma, time)
        self.assertEqual(str(round(test_val, 5)), "5.45325")
        
    def test_option_price_put_black_scholes(self):
        S = 50
        K = 50
        r = 0.10
        sigma = 0.30
        time = 0.50
        test_val = option_price_put_black_scholes(S, K, r, sigma, time)
        self.assertEqual(str(round(test_val, 5)), "3.01472")
        
    def test_option_price_delta_call_black_scholes(self):
        S = 50
        K = 50
        r = 0.10
        sigma = 0.30
        time = 0.50
        test_val = option_price_delta_call_black_scholes(S, K, r, sigma, time)
        self.assertEqual(str(round(test_val, 6)), "0.633737")
        
        
    def test_option_price_delta_put_black_scholes(self):
        S = 50
        K = 50
        r = 0.10
        sigma = 0.30
        time = 0.50
        test_val = option_price_delta_put_black_scholes(S, K, r, sigma, time)
        self.assertEqual(str(round(test_val, 6)), "-0.366263")
        
    def test_option_price_implied_volatility_call_black_scholes_bisections(self):
        S = 50
        K = 50
        r = 0.10
        time = 0.50
        option_price = 2.5
        test_val = option_price_implied_volatility_call_black_scholes_bisections(
                                                    S, K, r, time, option_price)
        self.assertEqual(str(round(test_val, 7)), "0.0500419")
        
        
    def test_option_price_implied_volatility_call_black_scholes_newton(self):
        S = 50
        K = 50
        r = 0.10
        time = 0.50
        option_price = 2.5
        test_val = option_price_implied_volatility_call_black_scholes_newton(S, 
                                                    K, r, time, option_price)
        self.assertEqual(str(round(test_val, 7)), "0.0500427")
        
    def test_option_price_partials_call_black_scholes(self):
        S = 50
        K = 50
        r = 0.10
        sigma = 0.30
        time = 0.50
        delta, gamma, theta, vega, rho = option_price_partials_call_black_scholes(S, K, r, sigma, time)
        self.assertEqual(str(round(delta, 6)), "0.633737")
        self.assertEqual(str(round(gamma, 7)), "0.0354789")
        self.assertEqual(str(round(theta, 5)), "-6.61473")
        self.assertEqual(str(round(vega, 4)), "13.3046")
        self.assertEqual(str(round(rho, 4)), "13.1168")
    

    def test_option_price_partials_put_black_scholes(self):
        S = 50
        K = 50
        r = 0.10
        sigma = 0.30
        time = 0.50
        delta, gamma, theta, vega, rho = option_price_partials_put_black_scholes(S, K, r, sigma, time)
        self.assertEqual(str(round(delta, 6)), "-0.366263")
        self.assertEqual(str(round(gamma, 7)), "0.0354789")
        self.assertEqual(str(round(theta, 5)), "-1.85859")
        self.assertEqual(str(round(vega, 4)), "13.3046")
        self.assertEqual(str(round(rho, 4)), "-10.6639")
        
        
    def test_option_price_european_call_payout(self):
        S = 100
        K = 100
        r = 0.1
        q = 0.05
        sigma = 0.25
        time = 1.0
        test_val = option_price_european_call_payout(S, K, r, q, sigma, time)
        self.assertEqual(str(round(test_val, 4)), "11.7344")
        
        
    def test_option_price_european_put_payout(self):
        S = 100
        K = 100
        r = 0.1
        q = 0.05
        sigma = 0.25
        time = 1.0
        test_val = option_price_european_put_payout(S, K, r, q, sigma, time)
        self.assertEqual(str(round(test_val, 5)), "7.09515")
        
        
    def test_option_price_european_call_dividends(self):
        S = 100
        K = 100
        r = 0.1
        sigma = 0.25
        time = 1.0
        dividend_times = [0.25, 0.75]
        dividend_amounts = [2.5, 2.5]
        test_val = option_price_european_call_dividends(S, K, r, sigma, 
                        time, dividend_times, dividend_amounts )
        self.assertEqual(str(round(test_val, 4)), "11.8094")
        
        
    def test_option_price_european_put_dividends(self):
        S = 100
        K = 100
        r = 0.1
        sigma = 0.25
        time = 1.0
        dividend_times = [0.25, 0.75]
        dividend_amounts = [2.5, 2.5]
        test_val = option_price_european_put_dividends(S, K, r, sigma, 
                        time, dividend_times, dividend_amounts )
        self.assertEqual(str(round(test_val, 5)), "7.05077")