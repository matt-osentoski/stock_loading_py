This document outlines the steps needed to load stock data into
a MySQL database.

1. If the database doesn't already exist, run /db/create.sql
1a. If the database exists, update the state of the 'stocks' table to active (a):
Use the following SQL:
UPDATE stocks SET state = 'a';

2a. Update /src/utils/initial_load/stock_importer.py. Set the 'end_loop' values
(~ lines 24  and 27) to the last page size for the components of the Nasdaq	and
NYSE.  (For example: http://finance.yahoo.com/q/cp?s=^IXIC+Components
shows 3937 pages at 50 entries per page.  Round this value up to the nearest 50,
so the value for 'end_loop' will be 3950 for the Nasdaq.
2b. Run /src/utils/initial_load/stock_importer.py.   This script will update new and inactive stocks
for the NYSE and NASDAQ Composite indexes.
2c. Review the log file and check for any errors.

3. Make sure there are no prices after the starting date that you will be using
in the next step. For example: if the last data process was through 6/31/2010, 
make sure there are no prices after this date that would create duplicates.
Run the following SQL, modifying the date to the appropriate value:
DELETE FROM stock_prices WHERE price_date > '2010-06-31' AND
symbol IN (SELECT symbol FROM stocks WHERE state = 'a' AND active = '1');

4a. Modify /src/utils/initial_load/load_data_with_yahoo.py to the date range you're looking for. 
4b. Run load_data_with_yahoo.py to grab the prices.  (This could take a long time)
4c. Check the log file that's created for any errors

5. Run the following script to see if any stocks are missing historical prices:
SELECT s.symbol, count(sp.symbol) as num FROM stocks s LEFT JOIN stock_prices sp ON s.symbol = sp.symbol GROUP BY s.symbol HAVING count(sp.symbol) = 0

5a. After verifying that the stocks don't have historical prices on Yahoo, run the following two scripts to delete this stocks:
CREATE TEMPORARY TABLE tmptable
SELECT s.symbol, count(sp.symbol) as num 
FROM stocks s LEFT JOIN stock_prices sp ON s.symbol = sp.symbol GROUP BY s.symbol HAVING count(sp.symbol) = 0;
-- Finally delete the entries
DELETE stocks FROM stocks INNER JOIN tmptable ON stocks.symbol = tmptable.symbol;

6. Run /src/utils/initial_load/treasury_importer.py to update the treasuries 
table.

7. Make a backup dump of this database

8. Reset the state of all stocks to available (a) again:
UPDATE stocks SET state = 'a';

9. In the 'stock_loading_java' module, run the following Java class:
scripts/UpdateWithTechIndicators
(NOTE: You can run multiple instances of this at the same time.  Normally
this would be one instance per processor available)

10. Make a backup dump of this database. 