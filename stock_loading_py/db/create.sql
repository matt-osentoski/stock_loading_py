-- To avoid constraint problems during this script, turn foreign
-- key checks off.
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS stocks;
CREATE TABLE stocks (
	id int not null auto_increment,
	symbol varchar(50),
	name varchar(255),
	description text,
	sector varchar(255),
	industry varchar(255),
	stock_index varchar(50),
	is_sp500 varchar(1) DEFAULT '0', -- (1) if this stock is a member of the S&P 500
	is_sp1500 varchar(1) DEFAULT '0', -- (1) if this stock is a member of the S&P 1500
	is_dow_ind varchar(1) DEFAULT '0', -- (1) if this stock is a member of the Dow Jones Industrials
	state varchar(1) DEFAULT 'n',   -- (n)ew, (a)vailable, (p)ending, (f)inished
	active varchar(1) DEFAULT '1', -- (1) if this stock is currently active.
	primary key (id)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS stock_prices;
CREATE TABLE stock_prices (
	id int not null auto_increment,
	symbol varchar(50),
	price_date datetime, 
	opening_price decimal(10,2), 
	high_price decimal(10,2), 
	low_price decimal(10,2), 
	closing_price decimal(10,2),
	adj_closing_price decimal(10,2), 
	period varchar(1),
	volume int, 
	sma_5 decimal(10,2), 
	sma_10 decimal(10,2), 
	sma_20 decimal(10,2),
	sma_50 decimal(10,2), 
	sma_100 decimal(10,2), 
	sma_200 decimal(10,2),
	ema_5 decimal(10,2), 
	ema_10 decimal(10,2), 
	ema_20 decimal(10,2),
	ema_50 decimal(10,2), 
	ema_100 decimal(10,2), 
	ema_200 decimal(10,2),
	ema_12 decimal(10,2), 
	ema_26 decimal(10,2), 
	macd_12_26 decimal(10,2), 
	macd_12_26_signal_9 decimal(10,2),
	macd_hist_12_26 decimal(10,2),
	ppo_12_26 decimal(10,5), 
	ppo_12_26_signal_9 decimal(10,5), 
	ppo_hist_12_26 decimal(10,5), 
	rsi_14 decimal(10,2), 
	rsi_20 decimal(10,2),
	rsi_30 decimal(10,2),
	stoch_fast_k_10_3 decimal(10,2), 
	stoch_fast_d_10_3 decimal(10,2),
	stoch_slow_k_10_3 decimal(10,2), 
	stoch_slow_d_10_3 decimal(10,2),
	stoch_fast_k_14_3 decimal(10,2), 
	stoch_fast_d_14_3 decimal(10,2),
	stoch_slow_k_14_3 decimal(10,2), 
	stoch_slow_d_14_3 decimal(10,2),
	vol_sma_5 int,
	vol_sma_10 int, 
	vol_sma_20 int,
	vol_sma_50 int, 
	vol_sma_100 int, 
	vol_sma_200 int,
	atr_14 decimal(10,2),
	hist_vola_20_252 decimal(10,2),
	primary key (id)
) ENGINE = InnoDB;


-- Historical Treasury rates
DROP TABLE IF EXISTS treasuries;
CREATE TABLE treasuries (
	id int not null auto_increment,
	time_period datetime,
	1_month decimal(10,2),  -- Values are percents
	3_month decimal(10,2),  -- Values are percents
	6_month decimal(10,2),  -- Values are percents
	1_year decimal(10,2),  -- Values are percents
	2_year decimal(10,2),  -- Values are percents
	3_year decimal(10,2),  -- Values are percents
	5_year decimal(10,2),  -- Values are percents
	7_year decimal(10,2),  -- Values are percents
	10_year decimal(10,2),  -- Values are percents
	20_year decimal(10,2),  -- Values are percents
	30_year decimal(10,2),  -- Values are percents
	primary key (id)
) ENGINE = InnoDB;


-- Create an index to increase performance when searching by stock_id

-- CREATE INDEX idx_stock_prices_symbol ON stock_prices (symbol);
-- CREATE INDEX idx_stock_prices_symbol_period ON stock_prices (symbol, period);

